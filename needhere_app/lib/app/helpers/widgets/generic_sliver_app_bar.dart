import 'package:flutter/material.dart';

class GenericSliverAppBarWidget extends StatelessWidget {
  const GenericSliverAppBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: 260,
      excludeHeaderSemantics: true,
      automaticallyImplyLeading: false,
      pinned: true,
      backgroundColor: Colors.black,
      flexibleSpace: FlexibleSpaceBar(
        background: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Image.asset(
            'assets/images/logo.png',
            height: 90,
          ),
        ),
        collapseMode: CollapseMode.parallax,
        centerTitle: true,
      ),
    );
  }
}
