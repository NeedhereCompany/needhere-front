import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/state.dart';
import 'package:needhere_app/modules/user/presentation/cubit/validate/cubit.dart';

class UserBuilder extends StatefulWidget {
  final Widget Function(BuildContext context, UserModel? user) builder;

  const UserBuilder({
    Key? key,
    required this.builder,
  }) : super(key: key);

  @override
  State<UserBuilder> createState() => _UserBuilderState();
}

class _UserBuilderState extends State<UserBuilder> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ValidateCubit, AuthenticateState>(
      listener: (context, state) {},
      builder: (context, state) {
        if (state is AuthenticatedState) {
          return widget.builder(context, state.user);
        } else {
          return widget.builder(context, null);
        }
      },
    );
  }
}
