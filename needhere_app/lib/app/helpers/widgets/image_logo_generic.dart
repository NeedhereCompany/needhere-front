import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class ImageLogoGeneric extends StatelessWidget {
  final bool isBack;

  const ImageLogoGeneric({
    Key? key,
    this.isBack = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      color: Colors.black,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(40.0),
            child: Image.asset(
              'assets/images/logo.png',
              fit: BoxFit.fill,
            ),
          ),
          isBack
              ? InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Icon(LineIcons.arrowLeft),
                  ),
                )
              : const SizedBox.shrink()
        ],
      ),
    );
  }
}
