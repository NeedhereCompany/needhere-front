import 'package:flutter/material.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

import 'progress_indicator.dart';

class NHButton extends StatelessWidget {
  final String title;
  final bool disabled;
  final Function()? onTap;
  final Color? backgroundColor;
  final bool isExpanded;
  final double? fontSize;
  final bool isRounded;
  final bool isLoading;

  const NHButton({
    Key? key,
    required this.title,
    required this.disabled,
    required this.onTap,
    this.backgroundColor,
    this.isExpanded = true,
    this.fontSize,
    this.isRounded = false,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: isExpanded ? double.infinity : null,
      height: 50,
      child: ElevatedButton(
        onPressed: isLoading ? null : onTap,
        style: ButtonStyle(
          backgroundColor: MaterialStateColor.resolveWith(
            (states) => backgroundColor ?? NHColors.secondaryColor,
          ),
          shape: isRounded == true
              ? MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                )
              : null,
        ),
        child: isLoading
            ? const NHProgressIndicator()
            : Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: fontSize ?? 14,
                ),
              ),
      ),
    );
  }
}
