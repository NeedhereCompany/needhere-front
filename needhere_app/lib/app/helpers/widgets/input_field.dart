import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NHInputField extends StatelessWidget {
  final TextEditingController controller;
  final String name;
  final String? placeHolder;
  final Widget? leading;
  final Widget? trailing;
  final Widget? preffixIcon;
  final Function()? trailingPressed;
  final bool isRequired;
  final bool isEmail;
  final bool isPassword;
  final TextInputType? textInputType;
  final int? maxLength;
  final bool isSquareBorder;
  final dynamic borderType;

  const NHInputField({
    Key? key,
    required this.name,
    required this.controller,
    this.placeHolder,
    this.leading,
    this.trailing,
    this.trailingPressed,
    this.isPassword = false,
    this.preffixIcon,
    this.textInputType,
    this.isRequired = false,
    this.isEmail = false,
    this.maxLength,
    this.isSquareBorder = false,
    this.borderType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: name,
      style: TextStyle(
        fontSize: 15,
        color: borderType == UnderlineInputBorder
            ? NHColors.primaryGreyColor
            : Colors.white,
      ),
      keyboardType: isEmail
          ? TextInputType.emailAddress
          : textInputType ?? TextInputType.text,
      obscureText: isPassword,
      maxLength: maxLength,
      decoration: InputDecoration(
        hintText: placeHolder,
        filled: true,
        fillColor: borderType == UnderlineInputBorder
            ? Colors.transparent
            : NHColors.primaryGreyColor,
        isDense: true,
        prefixIconConstraints: preffixIcon != null
            ? const BoxConstraints(
                minHeight: 20,
                minWidth: 20,
              )
            : null,
        prefixIcon: preffixIcon != null
            ? Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: SizedBox(
                  height: 18,
                  child: preffixIcon,
                ),
              )
            : null,
        suffixIconConstraints: const BoxConstraints(
          minHeight: 20,
          minWidth: 20,
        ),
        suffixIcon: trailing != null
            ? Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: SizedBox(
                  height: 18,
                  child: InkWell(
                    onTap: trailingPressed,
                    child: trailing,
                  ),
                ),
              )
            : null,
        border: isSquareBorder
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(0),
                borderSide: const BorderSide(
                  color: Colors.transparent,
                ),
              )
            : borderType == UnderlineInputBorder
                ? const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: NHColors.primaryGreyColor,
                    ),
                  )
                : OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: const BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
        focusedBorder: isSquareBorder
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(0),
                borderSide: const BorderSide(
                  color: Colors.transparent,
                ),
              )
            : borderType == UnderlineInputBorder
                ? const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: NHColors.primaryGreyColor,
                    ),
                  )
                : OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: const BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
        enabledBorder: isSquareBorder
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(0),
                borderSide: const BorderSide(
                  color: Colors.transparent,
                ),
              )
            : borderType == UnderlineInputBorder
                ? const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: NHColors.primaryGreyColor,
                    ),
                  )
                : OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: const BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
      ),
      validator: FormBuilderValidators.compose(
        [
          if (isRequired)
            FormBuilderValidators.required(
                errorText:
                    '$placeHolder ${AppLocalizations.of(context)!.isRequired.toString()}.'),
          if (isEmail)
            FormBuilderValidators.email(
                errorText:
                    '${AppLocalizations.of(context)!.isEmailValidator}.'),
        ],
      ),
    );
  }
}
