class AdapterTransform<T> {
  final String field;
  final T Function(dynamic data) callback;

  AdapterTransform({
    required this.field,
    required this.callback,
  });
}
