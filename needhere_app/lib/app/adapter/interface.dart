abstract class IAdapter {
  Map<String, dynamic> adapt(Map<String, dynamic> data);
  Map<String, dynamic> unfit(Map<String, dynamic> data);
}
