import 'package:dio/dio.dart';
import 'http.dart';

class HttpBuilder {
  String? _baseUrl;

  final List<InterceptorsWrapper> _interceptors = [];

  HttpBuilder setBaseUrl(String baseUrl) {
    _baseUrl = baseUrl;

    return this;
  }

  HttpBuilder addInterceptor(InterceptorsWrapper interceptor) {
    _interceptors.add(interceptor);

    return this;
  }

  IHttp build() {
    final dio = Dio(
      BaseOptions(
        baseUrl: _baseUrl!,
      ),
    );

    for (var interceptor in _interceptors) {
      dio.interceptors.add(interceptor);
    }

    return Http(dio);
  }
}
