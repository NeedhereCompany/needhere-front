import 'package:dio/dio.dart';
import 'package:needhere_app/modules/user/data/services/auth_storage.dart';

class TokenInterceptor extends InterceptorsWrapper {
  final AuthStorageService authStorageService;

  TokenInterceptor({required this.authStorageService});

  @override
  Future onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    print(options.method.runtimeType);

    if (!options.path.contains('/auth') &&
        !(options.path.contains('/users') && options.method == 'POST')) {
      options.headers = {
        'x-auth-token': authStorageService.getToken() ?? '',
      };
    }

    return super.onRequest(options, handler);
  }
}
