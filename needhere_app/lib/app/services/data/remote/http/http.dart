import 'package:dio/dio.dart';

class Http implements IHttp {
  final Dio _dio;

  Http(this._dio);

  @override
  Future<Response> get(
    String path, {
    String? baseUrl,
    Map<String, dynamic>? queryParams,
    cache = false,
    int? timeout,
  }) {
    final copyWithdDio = _dio.options.copyWith(baseUrl: baseUrl);

    _dio.options = copyWithdDio;

    return _dio.get(
      path,
      queryParameters: queryParams,
    );
  }

  @override
  Future<Response> post(
    String path,
    data, {
    String? baseUrl,
    Map<String, dynamic>? queryParams,
    Function(int sent, int total)? onUploadProgress,
  }) {
    final copyWithdDio = _dio.options.copyWith(baseUrl: baseUrl);

    _dio.options = copyWithdDio;

    return _dio.post(
      path,
      data: data,
      queryParameters: queryParams,
      onSendProgress: onUploadProgress,
    );
  }

  @override
  Future<Response> patch(
    String path,
    data, {
    String? baseUrl,
    Map<String, dynamic>? queryParams,
    Function(int sent, int total)? onUploadProgress,
  }) {
    final copyWithdDio = _dio.options.copyWith(baseUrl: baseUrl);

    _dio.options = copyWithdDio;

    return _dio.patch(
      path,
      data: data,
      queryParameters: queryParams,
      onSendProgress: onUploadProgress,
    );
  }

  @override
  Future<Response> put(
    String path,
    data, {
    String? baseUrl,
    Map<String, dynamic>? queryParams,
    Function(int sent, int total)? onUploadProgress,
  }) {
    final copyWithdDio = _dio.options.copyWith(baseUrl: baseUrl);

    _dio.options = copyWithdDio;

    return _dio.put(
      path,
      data: data,
      queryParameters: queryParams,
      onSendProgress: onUploadProgress,
    );
  }

  @override
  Future<Response> delete(
    String path, {
    String? baseUrl,
    Map<String, dynamic>? queryParams,
  }) {
    final copyWithdDio = _dio.options.copyWith(baseUrl: baseUrl);

    _dio.options = copyWithdDio;

    return _dio.delete(path, queryParameters: queryParams);
  }
}

abstract class IHttp {
  Future<Response> get(
    String path, {
    String baseUrl,
    Map<String, dynamic> queryParams,
  });

  Future<Response> post(
    String path,
    dynamic data, {
    String baseUrl,
    Map<String, dynamic> queryParams,
    Function(int sent, int total) onUploadProgress,
  });

  Future<Response> put(
    String path,
    dynamic data, {
    String baseUrl,
    Map<String, dynamic> queryParams,
    Function(int sent, int total) onUploadProgress,
  });

  Future<Response> patch(
    String path,
    dynamic data, {
    String baseUrl,
    Map<String, dynamic> queryParams,
    Function(int sent, int total) onUploadProgress,
  });

  Future<Response> delete(
    String path, {
    String baseUrl,
    Map<String, dynamic> queryParams,
  });
}
