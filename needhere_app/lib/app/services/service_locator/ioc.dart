part of 'service_providers.dart';

class IoC {
  static IoC container = IoC();

  static final _getIt = GetIt.instance;

  static T get<T extends Object>() {
    return _getIt.get<T>();
  }

  void register<T>(instance) {
    _getIt.registerSingleton<Object>(instance);
  }

  void registerAsync<T extends Object>(Future<T> Function() factoryFunction) {
    _getIt.registerSingletonAsync<T>(factoryFunction);
  }

  // void registerWithDependencies<T>(
  //   Function() factoryFunction, {
  //   Iterable<Object>? dependsOn,
  // }) {
  //   _getIt.registerSingletonWithDependencies<T>(
  //     factoryFunction,
  //     dependsOn: dependsOn,
  //   );
  // }

  void registerFactory<T extends Object>(T Function() factoryFunction) {
    _getIt.registerFactory<T>(factoryFunction);
  }

  void registerFactoryAsync<T extends Object>(
      Future<T> Function() factoryFunction) {
    _getIt.registerFactoryAsync<T>(factoryFunction);
  }

  void registerLazySingleton<T extends Object>(T Function() factoryFuntion) {
    _getIt.registerLazySingleton<T>(factoryFuntion);
  }

  static void onReady(callback) {
    _getIt.allReady().then(callback);
  }
}
