library service_provider;

import 'package:get_it/get_it.dart';

part 'ioc.dart';

class ServiceProviders {
  final List<ServiceProvider> providers;

  ServiceProviders(this.providers);

  static final _container = IoC.container;

  Future<void> boot() async {
    for (var provider in providers) {
      provider.register(_container);
    }
  }

  void dispose() {
    providers.clear();
  }
}

abstract class ServiceProvider {
  void register(IoC container);
}
