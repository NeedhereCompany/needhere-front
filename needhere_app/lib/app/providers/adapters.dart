import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/modules/post/data/adapters/post.dart';
import 'package:needhere_app/modules/user/data/adapters/user.dart';

class AdaptersServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton(
      () => UserAdapter(),
    );

    container.registerLazySingleton(
      () => PostAdapter(),
    );
  }
}
