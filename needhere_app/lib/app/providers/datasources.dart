import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/modules/post/data/sources/remote/post/implementation.dart';
import 'package:needhere_app/modules/post/data/sources/remote/post/interface.dart';
import 'package:needhere_app/modules/user/data/sources/local/user/implementation.dart';
import 'package:needhere_app/modules/user/data/sources/local/user/interface.dart';
import 'package:needhere_app/modules/user/data/sources/remote/user/implementation.dart';
import 'package:needhere_app/modules/user/data/sources/remote/user/interface.dart';

class DataSourcesServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton(
      () => UserRemoteDatasource(
        http: GetIt.I(),
        authStorageService: GetIt.I(),
        userAdapter: GetIt.I(),
      ),
    );

    container.registerLazySingleton<IUserRemoteDataSource>(
      () => UserRemoteDatasource(
        http: GetIt.I(),
        authStorageService: GetIt.I(),
        userAdapter: GetIt.I(),
      ),
    );

    container.registerLazySingleton(
      () => UserLocalDatasource(
        authStorageService: GetIt.I(),
      ),
    );

    container.registerLazySingleton<IUserLocalDataSource>(
      () => UserLocalDatasource(
        authStorageService: GetIt.I(),
      ),
    );

    container.registerLazySingleton(
      () => PostRemoteDataSource(
        http: GetIt.I(),
        postAdapter: GetIt.I(),
      ),
    );

    container.registerLazySingleton<IPostRemoteDataSource>(
      () => PostRemoteDataSource(
        http: GetIt.I(),
        postAdapter: GetIt.I(),
      ),
    );
  }
}
