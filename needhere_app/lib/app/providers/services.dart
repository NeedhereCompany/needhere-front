import 'package:get_it/get_it.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/modules/user/data/services/auth_storage.dart';

class ExtraServicesServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton(
      () => AuthStorageService(sharedPreferences: GetIt.I()),
    );
  }
}
