import 'dart:io';

import 'package:dio/dio.dart';

class DioOptions {
  static Options dioOptions() {
    // dynamic _userInfo = (sharedPreferences.get(CACHED_USER));
    // _userInfo = json.decode(_userInfo);
    // final _token = _userInfo['token'];

    var options = Options(headers: {
      // HttpHeaders.authorizationHeader: 'Bearer $_token',
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.contentTypeHeader: 'application/json'
    });

    return options;
  }
}
