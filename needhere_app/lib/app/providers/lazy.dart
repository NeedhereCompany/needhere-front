import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LazyServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton<SharedPreferences>(
      () => sharedPreferences!,
    );
  }
}
