import 'package:get_it/get_it.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/cubit.dart';
import 'package:needhere_app/modules/user/presentation/cubit/register/cubit.dart';
import 'package:needhere_app/modules/user/presentation/cubit/validate/cubit.dart';

import '../../modules/user/presentation/cubit/logout/cubit.dart';

class CubitServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerFactory(
      () => AuthenticateCubit(
        authenticateUseCase: GetIt.instance(),
      ),
    );

    container.registerFactory(
      () => ValidateCubit(
        validateUseCase: GetIt.instance(),
      ),
    );

    container.registerFactory(
      () => LogoutCubit(
        logoutUseCase: GetIt.instance(),
      ),
    );

    container.registerFactory(
      () => RegisterCubit(
        registerUseCase: GetIt.instance(),
      ),
    );
  }
}
