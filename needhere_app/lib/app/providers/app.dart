import 'package:get_it/get_it.dart';
import 'package:needhere_app/app/network/info.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';

class AppServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton<INetworkInfo>(() => NetworkInfo(GetIt.I()));

    container.registerLazySingleton(() => NetworkInfo(GetIt.I()));
  }
}
