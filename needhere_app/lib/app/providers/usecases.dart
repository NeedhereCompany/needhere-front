// import 'package:get_it/get_it.dart';
// import 'package:needhere_app/app/services/service_locator/service_providers.dart';
// import 'package:needhere_app/modules/post/domain/usecases/get_posts.dart';
// import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';

// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:needhere_app/modules/user/domain/usecases/logout.dart';
// import 'package:needhere_app/modules/user/domain/usecases/register.dart';
// import 'package:needhere_app/modules/user/domain/usecases/validate.dart';

// import 'repositories.dart';

// User
import 'package:get_it/get_it.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/modules/post/domain/usecases/get_posts.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';
import 'package:needhere_app/modules/user/domain/usecases/logout.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';
import 'package:needhere_app/modules/user/domain/usecases/validate.dart';

class UseCasesServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton(
      () => AuthenticateUseCase(GetIt.I()),
    );

    container.registerLazySingleton(
      () => RegisterUseCase(GetIt.I()),
    );

    container.registerLazySingleton(
      () => ValidateUseCase(GetIt.I()),
    );

    container.registerLazySingleton(
      () => LogoutUseCase(GetIt.instance()),
    );

    container.registerLazySingleton(
      () => GetPostsUseCase(GetIt.I()),
    );
  }
}
