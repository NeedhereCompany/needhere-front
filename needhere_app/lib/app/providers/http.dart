import 'package:dio/dio.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:needhere_app/app/services/data/remote/http/builder.dart';
import 'package:needhere_app/app/services/data/remote/http/http.dart';
import 'package:needhere_app/app/services/data/remote/http/interceptors/headers.dart';
import 'package:needhere_app/app/services/data/remote/http/interceptors/token.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/main.dart';
import 'package:needhere_app/modules/user/data/services/auth_storage.dart';

class HttpServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    var baseUrl = 'http://backend-needhere.herokuapp.com/api';

    container.registerLazySingleton<Dio>(() => Dio());
    container.registerLazySingleton(() => InternetConnectionChecker());
    // container.registerLazySingleton(() => DioConnectivityRequestRetrier(
    //     connectivity: GetIt.I.get<InternetConnectionChecker>(),
    //     dio: (GetIt.I.get<Dio>())));

    container.registerLazySingleton<IHttp>(() => HttpBuilder()
        .setBaseUrl(baseUrl)
        .addInterceptor(HeadersInterceptor())
        .addInterceptor(TokenInterceptor(
            authStorageService: AuthStorageService(
          sharedPreferences: sharedPreferences!,
        )))
        // .addInterceptor(RetryOnConnectionChangeInterceptor(
        //     requestRetrier: GetIt.I.get<DioConnectivityRequestRetrier>()))
        .build());
  }
}
