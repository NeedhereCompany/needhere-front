import 'package:get_it/get_it.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:needhere_app/modules/post/data/repositories/post.dart';
import 'package:needhere_app/modules/post/domain/repositories/post.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';
import 'package:needhere_app/modules/user/domain/repositories/user.dart';

class RepositoryServiceProvider extends ServiceProvider {
  @override
  void register(IoC container) {
    container.registerLazySingleton(
      () => UserRepository(
        remoteDataSource: GetIt.I(),
        localDataSource: GetIt.I(),
        connectivity: GetIt.I(),
      ),
    );

    container.registerLazySingleton<IUserRepository>(
      () => UserRepository(
        remoteDataSource: GetIt.I(),
        localDataSource: GetIt.I(),
        connectivity: GetIt.I(),
      ),
    );

    container.registerLazySingleton(
      () => PostRepository(
        remoteDataSource: GetIt.I(),
        connectivity: GetIt.I(),
      ),
    );

    container.registerLazySingleton<IPostRepository>(
      () => PostRepository(
        remoteDataSource: GetIt.I(),
        connectivity: GetIt.I(),
      ),
    );
  }
}
