import 'package:flutter/widgets.dart';
import 'package:needhere_app/modules/screens/chat/screen.dart';
import 'package:needhere_app/modules/screens/home/screen.dart';
import 'package:needhere_app/modules/user/presentation/screens/register/screen.dart';
import 'package:needhere_app/modules/screens/splash/screen.dart';
import 'package:needhere_app/modules/user/presentation/screens/login/screen.dart';

final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  SplashScreen.route: (context) => const SplashScreen(),
  LoginScreen.route: (context) => const LoginScreen(),
  HomeScreen.route: (context) => const HomeScreen(),
  RegisterScreen.route: (context) => const RegisterScreen(),
  ChatScreen.route: (context) => const ChatScreen(),
};
