import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {}

class GenericFailure extends Failure {
  final String message;
  GenericFailure({required this.message});

  @override
  List<Object?> get props => [message];
}
