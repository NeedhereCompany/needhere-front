import 'package:equatable/equatable.dart';

class ServerException extends Equatable implements Exception {
  final String message;

  const ServerException({required this.message});

  @override
  List<Object?> get props => [];
}

class CachedException extends Equatable implements Exception {
  @override
  List<Object?> get props => [];
}

class UnauthorizedException extends Equatable implements Exception {
  @override
  List<Object?> get props => [];
}

class ConnectionException extends Equatable implements Exception {
  @override
  List<Object?> get props => [];
}

class NotInternetException extends Equatable implements Exception {
  @override
  List<Object?> get props => [];
}

class LocaDataNotFoundException extends Equatable implements Exception {
  @override
  List<Object?> get props => [];
}

class LocalUnauthorizedException extends Equatable implements Exception {
  @override
  List<Object?> get props => [];
}
