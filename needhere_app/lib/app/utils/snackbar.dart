import 'package:flutter/material.dart';

void showSnackbar({
  required BuildContext context,
  required String message,
  Color? backgroundColor,
  Color? textColor = Colors.white,
  Duration? duration,
  SnackBarAction? action,
  Animation<double>? animation,
  double? elevation,
  void Function()? onVisible,
  SnackBarBehavior? behavior,
  GlobalKey<ScaffoldMessengerState>? scaffold,
}) {
  final snackbar = SnackBar(
    content: Builder(
      builder: (BuildContext context) {
        return Text(
          message,
          style: TextStyle(color: textColor),
        );
      },
    ),
    backgroundColor: backgroundColor ?? Theme.of(context).primaryColor,
    duration: duration ?? const Duration(milliseconds: 3000),
    action: action,
    animation: animation,
    elevation: elevation,
    onVisible: onVisible,
    behavior: behavior ?? SnackBarBehavior.floating,
  );

  if (scaffold != null) {
    scaffold.currentState!.hideCurrentSnackBar();

    scaffold.currentState!.showSnackBar(snackbar);
  } else {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }
}
