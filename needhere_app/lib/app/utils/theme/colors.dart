import 'package:flutter/material.dart';

class NHColors {
  static const Color primaryColor = Color(0xFF101010);
  static const Color primaryBlackDetailColor = Color(0xFF242424);
  static const Color primaryGreyColor = Color(0xFF555555);
  static const Color secondaryColor = Color(0xFF09B23F);
  static const Color whiteColor = Color(0xFFFFFFFF);
}
