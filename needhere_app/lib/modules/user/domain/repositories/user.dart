import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/exceptions.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/network/info.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';
import 'package:needhere_app/modules/user/data/sources/local/user/interface.dart';
import 'package:needhere_app/modules/user/data/sources/remote/user/interface.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';
import 'package:needhere_app/modules/user/domain/usecases/follow_user.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';

class UserRepository implements IUserRepository {
  final IUserRemoteDataSource remoteDataSource;
  final IUserLocalDataSource localDataSource;
  final NetworkInfo connectivity;

  UserRepository({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.connectivity,
  });

  @override
  Future<Either<Failure, UserModel>> authenticate(
      AuthenticateParams params) async {
    try {
      // if (await connectivity.isConnected) {
      final users = await remoteDataSource.authenticate(params);

      return Right(users);
      // }
      // return Left(
      //     GenericFailure(message: 'Você não está connectado a Internet'));
    } on ServerException catch (e) {
      return Left(GenericFailure(message: e.message.toString()));
    }
  }

  @override
  Future<Either<Failure, bool>> register(RegisterParams params) async {
    try {
      if (await connectivity.isConnected) {
        final users = await remoteDataSource.register(params);

        return Right(users);
      }
      return Left(
          GenericFailure(message: 'Você não está connectado a Internet'));
    } on ServerException catch (e) {
      return Left(GenericFailure(message: e.message.toString()));
    }
  }

  @override
  Future<Either<Failure, bool>> followUser(FollowUserParams params) async {
    try {
      if (await connectivity.isConnected) {
        final users = await remoteDataSource.followUser(params);

        return Right(users);
      }
      return Left(
          GenericFailure(message: 'Você não está connectado a Internet'));
    } on ServerException catch (e) {
      return Left(GenericFailure(message: e.message.toString()));
    }
  }

  @override
  Future<Either<Failure, UserModel?>> validate() async {
    try {
      // if (await connectivity.isConnected) {
      final users = await localDataSource.validate();

      return Right(users);
      // }
      // return Left(
      //     GenericFailure(message: 'Você não está connectado a Internet'));
    } on ServerException {
      return Left(GenericFailure(message: 'Erro na conexão com o Server'));
    }
  }

  @override
  Future<Either<Failure, bool>> logout() async {
    try {
      // if (await connectivity.isConnected) {
      final result = await localDataSource.logout();

      return Right(result);
      // }
      // return Left(
      //     GenericFailure(message: 'Você não está connectado a Internet'));
    } on ServerException {
      return Left(GenericFailure(message: 'Erro na conexão com o Server'));
    }
  }
}
