import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';

class AuthenticateUseCase extends UseCase<UserModel, AuthenticateParams> {
  final IUserRepository repository;

  AuthenticateUseCase(this.repository);
  @override
  Future<Either<Failure, UserModel>> call(AuthenticateParams params) async {
    return await repository.authenticate(params);
  }
}

class AuthenticateParams extends Equatable {
  final String needzer;
  final String password;

  const AuthenticateParams({
    required this.needzer,
    required this.password,
  });

  @override
  List<Object> get props => [needzer, password];
}
