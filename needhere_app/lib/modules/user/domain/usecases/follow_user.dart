import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';

class FollowUserUseCase extends UseCase<bool, FollowUserParams> {
  final IUserRepository repository;

  FollowUserUseCase(this.repository);
  @override
  Future<Either<Failure, bool>> call(FollowUserParams params) async {
    return await repository.followUser(params);
  }
}

class FollowUserParams extends Equatable {
  final bool isFollowing;
  final String followingId;
  final String followerId;

  const FollowUserParams({
    required this.isFollowing,
    required this.followingId,
    required this.followerId,
  });

  @override
  List<Object> get props => [isFollowing, followingId, followerId];
}
