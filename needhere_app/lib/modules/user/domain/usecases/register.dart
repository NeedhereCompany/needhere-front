import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';

class RegisterUseCase extends UseCase<bool, RegisterParams> {
  final IUserRepository repository;

  RegisterUseCase(this.repository);
  @override
  Future<Either<Failure, bool>> call(RegisterParams params) async {
    return await repository.register(params);
  }
}

class RegisterParams extends Equatable {
  final String name;
  final String email;
  final String password;
  final String phone;
  final String userName;

  const RegisterParams({
    required this.name,
    required this.email,
    required this.password,
    required this.phone,
    required this.userName,
  });

  @override
  List<Object> get props => [name, email, password, phone];
}
