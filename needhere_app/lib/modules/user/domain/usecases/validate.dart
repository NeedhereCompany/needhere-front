import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';

class ValidateUseCase extends UseCase<UserModel?, NoParams> {
  final IUserRepository repository;

  ValidateUseCase(this.repository);
  @override
  Future<Either<Failure, UserModel?>> call(NoParams params) async {
    return await repository.validate();
  }
}
