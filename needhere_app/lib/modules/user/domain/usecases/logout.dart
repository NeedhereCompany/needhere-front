import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/data/repositories/user.dart';

class LogoutUseCase extends UseCase<bool, NoParams> {
  final IUserRepository repository;

  LogoutUseCase(this.repository);
  @override
  Future<Either<Failure, bool>> call(NoParams params) async {
    return await repository.logout();
  }
}
