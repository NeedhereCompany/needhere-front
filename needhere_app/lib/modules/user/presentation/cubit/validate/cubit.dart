import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/domain/usecases/validate.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/state.dart';

class ValidateCubit extends Cubit<AuthenticateState> {
  final ValidateUseCase validateUseCase;

  ValidateCubit({
    required this.validateUseCase,
  }) : super(AuthInitialState());

  Future<void> validate() async {
    try {
      emit(AuthenticatingState());
      final validateOrFail = await validateUseCase.call(NoParams());

      validateOrFail.fold(
        (fail) {
          emit(AuthFailState(message: fail.props.first.toString()));
        },
        (user) {
          if (user != null) {
            emit(AuthenticatedState(user: user));
          } else {
            emit(AuthFailState(message: 'Error'));
          }
        },
      );
    } catch (e) {
      emit(AuthErrorState(message: e.toString()));
    }
  }
}
