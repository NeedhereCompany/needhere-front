import 'package:equatable/equatable.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';

abstract class AuthenticateState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthInitialState extends AuthenticateState {}

class AuthenticatingState extends AuthenticateState {}

class AuthenticatedState extends AuthenticateState {
  final UserModel user;
  AuthenticatedState({required this.user});
  @override
  List<Object> get props => [user];
}

class AuthFailState extends AuthenticateState {
  final String message;

  AuthFailState({required this.message});
  @override
  List<Object> get props => [message];
}

class AuthErrorState extends AuthenticateState {
  final String message;

  AuthErrorState({required this.message});
  @override
  List<Object> get props => [message];
}
