import 'state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';

class AuthenticateCubit extends Cubit<AuthenticateState> {
  final AuthenticateUseCase authenticateUseCase;

  AuthenticateCubit({
    required this.authenticateUseCase,
  }) : super(AuthInitialState());

  Future<void> authenticate({required AuthenticateParams params}) async {
    try {
      emit(AuthenticatingState());

      final authenticateOrfail = await authenticateUseCase.call(params);

      authenticateOrfail.fold(
        (fail) {
          emit(AuthFailState(message: fail.props.first.toString()));
        },
        (user) {
          emit(AuthenticatedState(user: user));
        },
      );
    } catch (e) {
      emit(AuthErrorState(message: e.toString()));
    }
  }
}
