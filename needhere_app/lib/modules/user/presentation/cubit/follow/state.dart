import 'package:equatable/equatable.dart';

abstract class FollowState extends Equatable {
  @override
  List<Object> get props => [];
}

class FollowInitialState extends FollowState {}

class FollowProcessingState extends FollowState {}

class FollowSuccessState extends FollowState {
  final bool isSuccess;

  FollowSuccessState({required this.isSuccess});
  @override
  List<Object> get props => [isSuccess];
}

class FollowFailedState extends FollowState {
  final String message;

  FollowFailedState({required this.message});
  @override
  List<Object> get props => [message];
}

class FollowErrorState extends FollowState {
  final String message;

  FollowErrorState({required this.message});
  @override
  List<Object> get props => [message];
}
