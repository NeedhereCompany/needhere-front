import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/modules/user/domain/usecases/follow_user.dart';

import 'state.dart';

class FollowNotifier extends Cubit<FollowState> {
  FollowNotifier({
    required this.registerUseCase,
  }) : super(FollowInitialState());

  final FollowUserUseCase registerUseCase;

  Future<void> followUser(FollowUserParams params) async {
    try {
      emit(FollowProcessingState());

      final followOrFail = await registerUseCase.call(params);

      followOrFail.fold(
        (fail) {
          emit(FollowFailedState(message: fail.props.first.toString()));
        },
        (result) {
          emit(FollowSuccessState(isSuccess: result));
        },
      );
    } catch (e) {
      emit(FollowErrorState(message: e.toString()));
    }
  }

  Future<void> unFollowUser(FollowUserParams params) async {
    try {
      emit(FollowProcessingState());

      final unFollowOrFail = await registerUseCase.call(params);

      unFollowOrFail.fold(
        (fail) {
          emit(FollowFailedState(message: fail.props.first.toString()));
        },
        (result) {
          emit(FollowSuccessState(isSuccess: result));
        },
      );
    } catch (e) {
      emit(FollowErrorState(message: e.toString()));
    }
  }
}
