import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/user/domain/usecases/logout.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/state.dart';

class LogoutCubit extends Cubit<AuthenticateState> {
  final LogoutUseCase logoutUseCase;

  LogoutCubit({
    required this.logoutUseCase,
  }) : super(AuthInitialState());

  Future<void> logout() async {
    try {
      emit(AuthenticatingState());

      final logoutOrFail = await logoutUseCase.call(NoParams());

      logoutOrFail.fold(
        (fail) {
          emit(AuthFailState(message: fail.props.first.toString()));
        },
        (success) {
          emit(AuthFailState(message: success.toString()));
        },
      );
    } catch (e) {
      emit(AuthErrorState(message: e.toString()));
    }
  }
}
