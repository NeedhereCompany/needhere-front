import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';
import 'package:needhere_app/modules/user/presentation/cubit/register/state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit({
    required this.registerUseCase,
  }) : super(RegisterInitialState());

  final RegisterUseCase registerUseCase;

  Future<void> register(RegisterParams params) async {
    try {
      emit(RegisteringState());

      final registerOrFail = await registerUseCase.call(params);

      registerOrFail.fold(
        (fail) {
          emit(RegisterFailedState(message: fail.props.first.toString()));
        },
        (right) {
          emit(RegisteredState(isRegistered: right));
        },
      );
    } catch (e) {
      emit(RegisterErrorState(message: e.toString()));
    }
  }

  @override
  Future<void> closeC() async {
    return super.close();
  }
}
