import 'package:equatable/equatable.dart';

abstract class RegisterState extends Equatable {
  @override
  List<Object> get props => [];
}

class RegisterInitialState extends RegisterState {}

class RegisteringState extends RegisterState {}

class RegisteredState extends RegisterState {
  final bool isRegistered;

  RegisteredState({required this.isRegistered});
  @override
  List<Object> get props => [isRegistered];
}

class RegisterFailedState extends RegisterState {
  final String message;

  RegisterFailedState({required this.message});
  @override
  List<Object> get props => [message];
}

class RegisterErrorState extends RegisterState {
  final String message;

  RegisterErrorState({required this.message});
  @override
  List<Object> get props => [message];
}
