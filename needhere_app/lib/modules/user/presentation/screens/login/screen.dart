import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:logger/logger.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/helpers/widgets/image_logo_generic.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/snackbar.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/forget_password/screen.dart';
import 'package:needhere_app/modules/screens/home/screen.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/cubit.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/state.dart';
import 'package:needhere_app/modules/user/presentation/cubit/validate/cubit.dart';
import 'package:needhere_app/modules/user/presentation/screens/register/screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';

class LoginScreen extends StatefulWidget {
  static String route = '/register';
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _fKey = GlobalKey<FormBuilderState>();
  var logger = Logger();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryBlackDetailColor,
        body: ListView(
          children: [
            const ImageLogoGeneric(),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Container(
                decoration: const BoxDecoration(
                  color: NHColors.primaryBlackDetailColor,
                ),
                child: FormBuilder(
                  key: _fKey,
                  autovalidateMode: AutovalidateMode.always,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            AppLocalizations.of(context)!.labelLogin,
                            style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      NHInputField(
                        name: 'needzer',
                        placeHolder: 'Needzer',
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/user.png',
                        ),
                        isRequired: true,
                        isSquareBorder: true,
                      ),
                      const SizedBox(height: 15),
                      NHInputField(
                        name: 'password',
                        placeHolder: AppLocalizations.of(context)!.password,
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/password.png',
                        ),
                        isPassword: true,
                        isRequired: true,
                        isSquareBorder: true,
                      ),
                      Row(
                        children: [
                          TextButton.icon(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) {
                                  return const ForgetPasswordScreen();
                                }),
                              );
                            },
                            icon: const Icon(
                              Icons.info_outline,
                              color: NHColors.primaryGreyColor,
                              size: 20,
                            ),
                            label: Text(
                              '${AppLocalizations.of(context)!.forgetPassword}?',
                              style: const TextStyle(
                                color: NHColors.whiteColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '${AppLocalizations.of(context)!.dontHaveAccount}? ',
                            style: const TextStyle(
                              color: Colors.white54,
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pushNamed(
                                context,
                                RegisterScreen.route,
                              );
                            },
                            child: Text(
                              AppLocalizations.of(context)!.signUp,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 50),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 80.0,
                          right: 80.0,
                        ),
                        child:
                            BlocConsumer<AuthenticateCubit, AuthenticateState>(
                                listener: (context, state) {
                          if (state is AuthenticatingState) {
                          } else if (state is AuthenticatedState) {
                            context.read<ValidateCubit>().validate();
                            Navigator.pushReplacementNamed(
                              context,
                              HomeScreen.route,
                            );
                          } else if (state is AuthFailState) {
                            showSnackbar(
                              context: context,
                              message: state.message,
                              backgroundColor: Colors.deepOrange,
                            );
                          } else if (state is AuthErrorState) {
                            showSnackbar(
                              context: context,
                              message: 'Ocorreu um erro!',
                              backgroundColor: Colors.deepOrange,
                            );
                          } else {
                            showSnackbar(
                              context: context,
                              message: 'Ocorreu um erro!',
                              backgroundColor: Colors.deepOrange,
                            );
                          }
                        }, builder: (context, state) {
                          return NHButton(
                            title: AppLocalizations.of(context)!.signIn,
                            disabled: true,
                            isLoading:
                                state is AuthenticatingState ? true : false,
                            onTap: () {
                              if (_fKey.currentState?.saveAndValidate() ??
                                  false) {
                                final data = _fKey.currentState?.value;

                                AuthenticateParams params = AuthenticateParams(
                                  needzer: data!['needzer'],
                                  password: data['password'],
                                );

                                context
                                    .read<AuthenticateCubit>()
                                    .authenticate(params: params);
                              }
                            },
                            isExpanded: true,
                          );
                        }),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
