import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/helpers/widgets/image_logo_generic.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/snackbar.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';
import 'package:needhere_app/modules/user/presentation/cubit/register/cubit.dart';
import 'package:needhere_app/modules/user/presentation/cubit/register/state.dart';
import 'package:needhere_app/modules/user/presentation/screens/login/screen.dart';

class RegisterScreen extends StatefulWidget {
  static String route = '/login';
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _fKey = GlobalKey<FormBuilderState>();
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryBlackDetailColor,
        body: ListView(
          shrinkWrap: true,
          children: <Widget>[
            const ImageLogoGeneric(isBack: true),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Container(
                decoration: const BoxDecoration(
                  color: NHColors.primaryBlackDetailColor,
                ),
                child: FormBuilder(
                  key: _fKey,
                  autovalidateMode: AutovalidateMode.always,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            AppLocalizations.of(context)!.signUp,
                            style: const TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      NHInputField(
                        name: 'name',
                        placeHolder: AppLocalizations.of(context)!.name,
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/user.png',
                        ),
                        isRequired: true,
                        isSquareBorder: true,
                      ),
                      const SizedBox(height: 15),
                      NHInputField(
                        name: 'needzer',
                        placeHolder: 'Needzer',
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/user.png',
                        ),
                        isRequired: true,
                        isSquareBorder: true,
                      ),
                      const SizedBox(height: 15),
                      NHInputField(
                        name: 'password',
                        placeHolder: AppLocalizations.of(context)!.password,
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/password.png',
                        ),
                        isPassword: true,
                        isRequired: true,
                        isSquareBorder: true,
                      ),
                      const SizedBox(height: 15),
                      NHInputField(
                        name: 'email',
                        placeHolder: 'E-mail',
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/email.png',
                        ),
                        isPassword: false,
                        isRequired: true,
                        isEmail: true,
                        isSquareBorder: true,
                      ),
                      const SizedBox(height: 15),
                      NHInputField(
                        name: 'phoneNumber',
                        placeHolder: AppLocalizations.of(context)!.phoneNumber,
                        controller: TextEditingController(),
                        trailing: Image.asset(
                          'assets/icons/phone.png',
                        ),
                        textInputType: TextInputType.number,
                        isPassword: false,
                        isRequired: true,
                        isSquareBorder: true,
                      ),
                      Theme(
                        data: ThemeData(
                          checkboxTheme: CheckboxThemeData(
                            fillColor: MaterialStateColor.resolveWith(
                              (states) {
                                if (states.contains(MaterialState.selected)) {
                                  return Colors.white;
                                }
                                return Colors.transparent;
                              },
                            ),
                            side: const BorderSide(
                              color: Colors.white,
                              width: 1.5,
                            ),
                            visualDensity: VisualDensity.compact,
                            checkColor: MaterialStateColor.resolveWith(
                                (states) => Colors.white),
                            shape: const CircleBorder(
                              side: BorderSide(
                                // style: BorderStyle.solid,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        child: FormBuilderCheckbox(
                          name: 'acceptTerms',
                          initialValue: false,
                          onChanged: (value) {},
                          title: Text(
                            AppLocalizations.of(context)!.labelTerms,
                            style: const TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          validator: FormBuilderValidators.equal(
                            true,
                            errorText: AppLocalizations.of(context)!
                                .labelTermosValidator,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 80.0,
                          right: 80.0,
                        ),
                        child: BlocConsumer<RegisterCubit, RegisterState>(
                            listener: (context, state) {
                          if (state is RegisteringState) {
                          } else if (state is RegisteredState) {
                            showSnackbar(
                              context: context,
                              message: 'Conta criada com sucesso!',
                              backgroundColor: Colors.green,
                            );

                            Navigator.pushReplacementNamed(
                              context,
                              LoginScreen.route,
                            );
                          } else if (state is RegisterFailedState) {
                            showSnackbar(
                              context: context,
                              message: state.message.toString(),
                              backgroundColor: Colors.red,
                            );
                          } else {}
                        }, builder: (context, state) {
                          return NHButton(
                            title: AppLocalizations.of(context)!
                                .createAccountLabel,
                            disabled: true,
                            isLoading: state is RegisteringState ? true : false,
                            onTap:

                                // state is RegisteringState
                                //     ? () => null
                                //     :

                                () {
                              print('object');

                              if (_fKey.currentState?.saveAndValidate() ??
                                  false) {
                                final data = _fKey.currentState?.value;

                                RegisterParams params = RegisterParams(
                                  name: data!['name'],
                                  email: data['email'],
                                  password: data['password'],
                                  phone: data['phoneNumber'],
                                  userName: data['needzer'],
                                );

                                context.read<RegisterCubit>().register(params);
                              }
                            },
                            fontSize: 12,
                            isExpanded: true,
                          );
                        }),
                      ),
                      const SizedBox(height: 20)
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
