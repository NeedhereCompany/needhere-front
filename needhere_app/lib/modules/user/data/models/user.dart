import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class UserModel {
  String id;
  String name;
  String email;
  String userName;
  String phone;
  String registerDate;
  String token;

  UserModel({
    required this.id,
    required this.name,
    required this.email,
    required this.userName,
    required this.phone,
    required this.registerDate,
    required this.token,
  });

  factory UserModel.fromJson(json) => _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
