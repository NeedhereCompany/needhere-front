import 'package:needhere_app/app/adapter/interface.dart';

class UserAdapter extends IAdapter {
  @override
  Map<String, dynamic> adapt(Map<String, dynamic> data) {
    return {
      'id': data['user']['_id'],
      'name': data['user']['name'],
      'email': data['user']['email'],
      'userName': data['user']['username'],
      'phone': data['user']['phone'],
      'registerDate': data['user']['register'],
      'token': data['token'] ?? '',
    };
  }

  @override
  Map<String, dynamic> unfit(Map<String, dynamic> data) {
    throw UnimplementedError();
  }
}
