import 'package:dio/dio.dart';
import 'package:needhere_app/app/error/exceptions.dart';
import 'package:needhere_app/app/services/data/remote/http/http.dart';
import 'package:needhere_app/modules/user/data/adapters/user.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/data/services/auth_storage.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';
import 'package:needhere_app/modules/user/domain/usecases/follow_user.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';
import 'interface.dart';

class UserRemoteDatasource implements IUserRemoteDataSource {
  final IHttp http;
  final AuthStorageService authStorageService;
  final UserAdapter userAdapter;

  UserRemoteDatasource({
    required this.http,
    required this.authStorageService,
    required this.userAdapter,
  });

  @override
  Future<UserModel> authenticate(AuthenticateParams params) async {
    try {
      final response = await http.post(
        '/auth',
        {
          'username': params.needzer,
          'password': params.password,
        },
      );

      var user = UserModel.fromJson(userAdapter.adapt(response.data));
      authStorageService.store(user: user);

      return user;
    } on DioError catch (e) {
      throw ServerException(
          message: e.response!.data.containsKey('message')
              ? e.response!.data['message']
              : e.response!.data['msg']);
    }
  }

  @override
  Future<bool> register(RegisterParams params) async {
    try {
      await http.post(
        '/users',
        {
          'name': params.name,
          'email': params.email,
          'password': params.password,
          'phone': params.phone,
          'username': params.userName,
        },
      );

      return true;
    } on DioError catch (e) {
      var messages = <String>[];

      print(e.response!.statusCode.toString());

      if (e.response!.data.containsKey('errors')) {
        e.response!.data['errors'].forEach(
          (error) {
            messages.add(error['msg']);
          },
        );
      }

      throw ServerException(
        message: e.response!.data.containsKey('errors')
            ? messages.join('\n').toString()
            : e.response!.data.containsKey('message')
                ? e.response!.data['message']
                : e.response!.data['msg'],
      );
    }
  }

  @override
  Future<bool> followUser(FollowUserParams params) async {
    try {
      await http.patch(
        '/users/${params.isFollowing ? 'follow' : 'unfollow'}',
        {'followingId': params.followingId, 'followerId': params.followerId},
      );

      return true;
    } on DioError catch (e) {
      throw ServerException(
          message: e.response!.data.containsKey('message')
              ? e.response!.data['message']
              : e.response!.data['msg']);
    }
  }
}
