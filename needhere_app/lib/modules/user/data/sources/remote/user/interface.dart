import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';
import 'package:needhere_app/modules/user/domain/usecases/follow_user.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';

abstract class IUserRemoteDataSource {
  Future<UserModel> authenticate(AuthenticateParams params);
  Future<bool> register(RegisterParams params);
  Future<bool> followUser(FollowUserParams params);
}
