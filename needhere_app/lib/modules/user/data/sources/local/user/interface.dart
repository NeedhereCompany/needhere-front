import 'package:needhere_app/modules/user/data/models/user.dart';

abstract class IUserLocalDataSource {
  Future<UserModel?> validate();
  Future<bool> logout();
}
