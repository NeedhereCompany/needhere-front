import 'package:needhere_app/modules/user/data/services/auth_storage.dart';

import 'interface.dart';
import 'package:dio/dio.dart';
import 'package:needhere_app/app/error/exceptions.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';

class UserLocalDatasource implements IUserLocalDataSource {
  final AuthStorageService authStorageService;

  UserLocalDatasource({
    required this.authStorageService,
  });

  @override
  Future<UserModel?> validate() async {
    try {
      var user = authStorageService.get();

      return user;
    } on DioError catch (e) {
      throw ServerException(
          message: e.response!.data.containsKey('message')
              ? e.response!.data['message']
              : e.response!.data['msg']);
    }
  }

  @override
  Future<bool> logout() async {
    try {
      authStorageService.remove();

      return true;
    } on DioError catch (e) {
      throw ServerException(
          message: e.response!.data.containsKey('message')
              ? e.response!.data['message']
              : e.response!.data['msg']);
    }
  }
}
