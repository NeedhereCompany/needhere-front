import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:needhere_app/modules/user/domain/usecases/authenticate.dart';
import 'package:needhere_app/modules/user/domain/usecases/follow_user.dart';
import 'package:needhere_app/modules/user/domain/usecases/register.dart';

abstract class IUserRepository {
  Future<Either<Failure, UserModel>> authenticate(AuthenticateParams params);
  Future<Either<Failure, bool>> register(RegisterParams params);
  Future<Either<Failure, bool>> followUser(FollowUserParams params);
  Future<Either<Failure, UserModel?>> validate();
  Future<Either<Failure, bool>> logout();
}
