import 'dart:convert';

import 'package:needhere_app/modules/user/data/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthStorageService {
  final SharedPreferences sharedPreferences;

  final authKey = 'auth';

  AuthStorageService({required this.sharedPreferences});

  Future<void> store({required UserModel user}) async {
    await sharedPreferences.setString(authKey, jsonEncode(user.toJson()));
  }

  UserModel? get() {
    return UserModel.fromJson(jsonDecode(
      sharedPreferences.getString(authKey)!,
    ));
  }

  String? getToken() {
    return get()!.token;
  }

  bool hasToken() {
    return sharedPreferences.containsKey(authKey);
  }

  Future<void> remove() async {
    await sharedPreferences.remove(authKey);
  }
}
