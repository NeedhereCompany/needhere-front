import 'package:equatable/equatable.dart';
import 'package:needhere_app/modules/post/data/models/post.dart';

abstract class PostState extends Equatable {
  @override
  List<Object> get props => [];
}

class PostInitialState extends PostState {}

class PostingState extends PostState {}

class PostGettedState extends PostState {
  final List<PostModel> posts;

  PostGettedState({required this.posts});
  @override
  List<Object> get props => [posts];
}

class PostFailedState extends PostState {
  final String message;

  PostFailedState({required this.message});
  @override
  List<Object> get props => [message];
}

class PostErrorState extends PostState {
  final String message;

  PostErrorState({required this.message});
  @override
  List<Object> get props => [message];
}
