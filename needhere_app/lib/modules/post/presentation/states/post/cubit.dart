import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/post/domain/usecases/get_posts.dart';

import 'state.dart';

class PostCubit extends Cubit<PostState> {
  PostCubit({
    required this.getPostsUseCase,
  }) : super(PostInitialState());

  final GetPostsUseCase getPostsUseCase;

  Future<void> getPosts() async {
    try {
      emit(PostingState());

      final getOrFail = await getPostsUseCase.call(NoParams());

      getOrFail.fold(
        (fail) {
          emit(PostFailedState(message: fail.props.first.toString()));
        },
        (posts) {
          emit(PostGettedState(posts: posts));
        },
      );
    } catch (e) {
      emit(PostErrorState(message: e.toString()));
    }
  }
}
