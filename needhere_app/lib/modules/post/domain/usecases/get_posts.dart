import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/usecases/usecase.dart';
import 'package:needhere_app/modules/post/data/models/post.dart';
import 'package:needhere_app/modules/post/data/repositories/post.dart';

class GetPostsUseCase extends UseCase<List<PostModel>, NoParams> {
  final IPostRepository repository;

  GetPostsUseCase(this.repository);
  @override
  Future<Either<Failure, List<PostModel>>> call(NoParams params) async {
    return await repository.getPosts();
  }
}
