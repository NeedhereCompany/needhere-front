import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/exceptions.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/app/network/info.dart';
import 'package:needhere_app/modules/post/data/models/post.dart';
import 'package:needhere_app/modules/post/data/repositories/post.dart';
import 'package:needhere_app/modules/post/data/sources/remote/post/interface.dart';

class PostRepository implements IPostRepository {
  final IPostRemoteDataSource remoteDataSource;
  final NetworkInfo connectivity;

  PostRepository({
    required this.remoteDataSource,
    required this.connectivity,
  });

  @override
  Future<Either<Failure, List<PostModel>>> getPosts() async {
    try {
      if (await connectivity.isConnected) {
        final posts = await remoteDataSource.getPosts();

        return Right(posts);
      }
      return Left(
          GenericFailure(message: 'Você não está connectado a Internet'));
    } on ServerException {
      return Left(GenericFailure(message: 'Erro na conexão com o Server'));
    }
  }
}
