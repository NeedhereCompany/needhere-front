// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostModel _$PostModelFromJson(Map<String, dynamic> json) => PostModel(
      id: json['id'] as String,
      description: json['description'] as String,
      createdDate: json['createdDate'] as String,
      likes: json['likes'] as int,
      creatorId: json['creatorId'] as String,
      pictures: (json['pictures'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$PostModelToJson(PostModel instance) => <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'createdDate': instance.createdDate,
      'likes': instance.likes,
      'creatorId': instance.creatorId,
      'pictures': instance.pictures,
    };
