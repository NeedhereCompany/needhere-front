import 'package:freezed_annotation/freezed_annotation.dart';

part 'post.g.dart';

@JsonSerializable()
class PostModel {
  String id;
  String description;
  String createdDate;
  int likes;
  String creatorId;
  List<String>? pictures;

  PostModel({
    required this.id,
    required this.description,
    required this.createdDate,
    required this.likes,
    required this.creatorId,
    required this.pictures,
  });

  factory PostModel.fromJson(json) => _$PostModelFromJson(json);
  Map<String, dynamic> toJson() => _$PostModelToJson(this);
}
