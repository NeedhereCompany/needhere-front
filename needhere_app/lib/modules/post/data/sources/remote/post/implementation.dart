import 'package:dio/dio.dart';
import 'package:needhere_app/app/error/exceptions.dart';
import 'package:needhere_app/app/services/data/remote/http/http.dart';
import 'package:needhere_app/modules/post/data/adapters/post.dart';
import 'package:needhere_app/modules/post/data/models/post.dart';
import 'interface.dart';

class PostRemoteDataSource implements IPostRemoteDataSource {
  final IHttp http;
  final PostAdapter postAdapter;

  PostRemoteDataSource({
    required this.http,
    required this.postAdapter,
  });

  @override
  Future<List<PostModel>> getPosts() async {
    try {
      var posts = <PostModel>[];

      // print('waweeee');

      final response = await http.get('/posts');

      response.data['posts']
          .map((post) => posts.add(PostModel.fromJson(postAdapter.adapt(post))))
          .toList();

      return posts;
    } on DioError catch (e) {
      throw ServerException(
          message: e.response!.data.containsKey('message')
              ? e.response!.data['message']
              : e.response!.data['msg']);
    }
  }
}
