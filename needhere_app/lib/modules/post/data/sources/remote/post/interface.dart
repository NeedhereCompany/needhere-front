import 'package:needhere_app/modules/post/data/models/post.dart';

abstract class IPostRemoteDataSource {
  Future<List<PostModel>> getPosts();
}
