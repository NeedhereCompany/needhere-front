import 'package:dartz/dartz.dart';
import 'package:needhere_app/app/error/failure.dart';
import 'package:needhere_app/modules/post/data/models/post.dart';

abstract class IPostRepository {
  Future<Either<Failure, List<PostModel>>> getPosts();
}
