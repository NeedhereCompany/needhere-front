import 'package:needhere_app/app/adapter/interface.dart';

class PostAdapter extends IAdapter {
  @override
  Map<String, dynamic> adapt(Map<String, dynamic> data) {
    var pictures = <String>[];

    data['pics'].forEach((picture) {
      pictures.add('$picture');
    });

    return {
      'id': data['_id'],
      'description': data['description'],
      'createdDate': data['created'],
      'likes': data['likes'],
      'creatorId': data['creator'],
      'pictures': pictures.isEmpty ? [] : pictures,
    };
  }

  @override
  Map<String, dynamic> unfit(Map<String, dynamic> data) {
    throw UnimplementedError();
  }
}
