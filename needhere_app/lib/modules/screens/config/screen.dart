import 'package:flutter/material.dart';
import 'package:needhere_app/modules/screens/config/pages/config_access/page.dart';

import 'widgets/option.dart';

class ConfigurationScreen extends StatefulWidget {
  const ConfigurationScreen({Key? key}) : super(key: key);

  @override
  _ConfigurationScreenState createState() => _ConfigurationScreenState();
}

class _ConfigurationScreenState extends State<ConfigurationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Configurações'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                OptionWidget(
                  title: 'Configurações da conta',
                  subTitle: 'Altere suas configurações de acesso',
                  onTap: () {},
                ),
                const SizedBox(height: 10),
                OptionWidget(
                  title: 'Configurações de acesso',
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return const ConfigurationAccessPage();
                      }),
                    );
                  },
                ),
              ],
            ),
          ),
          const Divider(thickness: 2),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                OptionWidget(
                  title: 'Notificações',
                  subTitle: 'Escolha quais notificações você deseja receber',
                  onTap: () {},
                ),
                const SizedBox(height: 10),
                OptionWidget(
                  title: 'Notificações por push',
                  onTap: () {},
                ),
              ],
            ),
          ),
          const Divider(thickness: 2),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                OptionWidget(
                  title: 'Suporte',
                  subTitle: 'Obtenha ajuda da nossa equipe de suporte',
                  onTap: () {},
                ),
                const SizedBox(height: 8),
                OptionWidget(
                  title: 'Ajuda e suporte',
                  onTap: () {},
                ),
                const SizedBox(height: 8),
                OptionWidget(
                  title: 'Termos de uso',
                  onTap: () {},
                ),
                const SizedBox(height: 8),
                OptionWidget(
                  title: 'Política de privacidade',
                  onTap: () {},
                )
              ],
            ),
          ),
          const Divider(thickness: 2),
        ],
      ),
    );
  }
}
