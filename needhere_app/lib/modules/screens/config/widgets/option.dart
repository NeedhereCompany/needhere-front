import 'package:flutter/material.dart';

class OptionWidget extends StatelessWidget {
  final String title;
  final String? subTitle;
  final Function onTap;

  const OptionWidget({
    Key? key,
    required this.title,
    this.subTitle,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 12.0),
        child: subTitle != null
            ? Text(
                subTitle!,
                style: const TextStyle(
                  fontSize: 13,
                ),
              )
            : const SizedBox.shrink(),
      ),
      onTap: () => onTap(),
    );
  }
}
