import 'package:flutter/material.dart';
import 'package:needhere_app/modules/screens/config/pages/config_access/pages/change_email/page.dart';
import 'package:needhere_app/modules/screens/config/pages/config_access/pages/change_password/page.dart';
import 'package:needhere_app/modules/screens/config/pages/config_access/pages/change_phone_number/page.dart';

class ConfigurationAccessPage extends StatefulWidget {
  const ConfigurationAccessPage({Key? key}) : super(key: key);

  @override
  _ConfigurationAccessPageState createState() =>
      _ConfigurationAccessPageState();
}

class _ConfigurationAccessPageState extends State<ConfigurationAccessPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Configurações de acesso'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            ListTile(
              title: const Text(
                'Endereço de email',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              subtitle: const Text(
                'contateste@gmail.com',
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              trailing: const Icon(
                Icons.arrow_forward_ios,
                color: Colors.white54,
                size: 15,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ChangeEmailPage()),
                );
              },
            ),
            const SizedBox(height: 10),
            ListTile(
              title: const Text(
                'Número de telefone',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              subtitle: const Text(
                '+55(21)9926-94036',
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              trailing: const Icon(
                Icons.arrow_forward_ios,
                color: Colors.white54,
                size: 15,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const ChangePhoneNumberPage()),
                );
              },
            ),
            const SizedBox(height: 10),
            ListTile(
              title: const Text(
                'Senha',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              subtitle: const Text(
                '**************',
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              trailing: const Icon(
                Icons.arrow_forward_ios,
                color: Colors.white54,
                size: 15,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ChangePasswordPage()),
                );
              },
            ),
            const SizedBox(height: 60),
            ListTile(
              title: const Text(
                'Excluir conta',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
              trailing: const Icon(
                Icons.arrow_forward_ios,
                color: Colors.white54,
                size: 15,
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}
