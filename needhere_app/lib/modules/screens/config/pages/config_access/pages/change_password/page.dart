import 'package:flutter/material.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Alterar senha'),
        actions: [
          TextButton(
            onPressed: () {},
            style: ButtonStyle(
                foregroundColor: MaterialStateColor.resolveWith(
                    (states) => NHColors.secondaryColor)),
            child: const Text('Próximo'),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 50.0,
          right: 50.0,
        ),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Insira sua nova senha',
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Digite e confirme uma nova senha usando o espaço abaixo  ',
                style: TextStyle(color: NHColors.whiteColor.withOpacity(0.5)),
              ),
              const SizedBox(height: 40),
              NHInputField(
                name: 'password',
                controller: TextEditingController(),
                placeHolder: 'Nova senha',
                borderType: UnderlineInputBorder,
                isPassword: true,
              ),
              const SizedBox(height: 20),
              NHInputField(
                name: 'confirmPassword',
                controller: TextEditingController(),
                placeHolder: 'Confirmar nova senha',
                borderType: UnderlineInputBorder,
                isPassword: true,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
