import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class ConfirmCodePage extends StatefulWidget {
  const ConfirmCodePage({Key? key}) : super(key: key);

  @override
  _ConfirmCodePageState createState() => _ConfirmCodePageState();
}

class _ConfirmCodePageState extends State<ConfirmCodePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Código de confirmação'),
        actions: [
          TextButton(
            onPressed: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) => ConfirmCodePage()),
              // );
            },
            style: ButtonStyle(
                foregroundColor: MaterialStateColor.resolveWith(
                    (states) => NHColors.secondaryColor)),
            child: const Text('Próximo'),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 50.0,
          right: 50.0,
        ),
        child: SizedBox(
          // width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Insira o código de confirmação',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Você receberá um código de confirmação em sey telefine',
                style: TextStyle(
                  color: NHColors.whiteColor.withOpacity(0.5),
                ),
              ),
              const SizedBox(height: 40),
              Padding(
                padding: const EdgeInsets.only(
                  left: 200.0,
                  right: 200.0,
                ),
                child: PinCodeTextField(
                  appContext: context,
                  length: 4,
                  animationType: AnimationType.fade,
                  enableActiveFill: true,
                  onChanged: (value) {},
                  cursorColor: Colors.white,
                  textStyle: const TextStyle(
                    color: Colors.grey,
                    fontSize: 15,
                  ),
                  keyboardType: TextInputType.number,
                  pinTheme: PinTheme(
                    selectedColor: Colors.transparent,
                    selectedFillColor: Colors.transparent,
                    inactiveColor: NHColors.primaryGreyColor,
                    inactiveFillColor: Colors.transparent,
                    activeColor: Colors.transparent,
                    activeFillColor: Colors.transparent,
                    shape: PinCodeFieldShape.circle,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
