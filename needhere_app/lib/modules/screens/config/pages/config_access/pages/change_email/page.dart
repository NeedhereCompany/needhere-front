import 'package:flutter/material.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/config/pages/config_access/pages/confirm_code/page.dart';

class ChangeEmailPage extends StatefulWidget {
  const ChangeEmailPage({Key? key}) : super(key: key);

  @override
  _ChangeEmailPageState createState() => _ChangeEmailPageState();
}

class _ChangeEmailPageState extends State<ChangeEmailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Alterar email'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ConfirmCodePage()),
              );
            },
            style: ButtonStyle(
                foregroundColor: MaterialStateColor.resolveWith(
                    (states) => NHColors.secondaryColor)),
            child: const Text('Próximo'),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 50.0,
          right: 50.0,
        ),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Insira seu novo endereço de email',
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Você receberá um email para confirmar seu novo endereço de email',
                style: TextStyle(color: NHColors.whiteColor.withOpacity(0.5)),
              ),
              const SizedBox(height: 40),
              NHInputField(
                name: 'email',
                controller: TextEditingController(),
                placeHolder: 'Novo endereço de email',
                borderType: UnderlineInputBorder,
                isEmail: true,
              ),
              const SizedBox(height: 20),
              Text(
                'Suas informações de contato são privadas e não serão exibidas às outras pessoas  ',
                style: TextStyle(color: NHColors.whiteColor.withOpacity(0.5)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
