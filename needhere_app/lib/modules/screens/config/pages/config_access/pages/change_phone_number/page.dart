import 'package:flutter/material.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class ChangePhoneNumberPage extends StatefulWidget {
  const ChangePhoneNumberPage({Key? key}) : super(key: key);

  @override
  _ChangePhoneNumberPageState createState() => _ChangePhoneNumberPageState();
}

class _ChangePhoneNumberPageState extends State<ChangePhoneNumberPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Número de telefone'),
        actions: [
          TextButton(
            onPressed: () {},
            style: ButtonStyle(
                foregroundColor: MaterialStateColor.resolveWith(
                    (states) => NHColors.secondaryColor)),
            child: const Text('Próximo'),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 50.0,
          right: 50.0,
        ),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Insira um novo número de telefone',
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'Você receberá um código de confirmação em seu telefone',
                style: TextStyle(color: NHColors.whiteColor.withOpacity(0.5)),
              ),
              const SizedBox(height: 40),
              NHInputField(
                name: 'email',
                controller: TextEditingController(),
                placeHolder: '+5521998161436',
                borderType: UnderlineInputBorder,
                isEmail: true,
                preffixIcon: Icon(
                  Icons.message,
                  color: Colors.white.withOpacity(0.4),
                ),
              ),
              const SizedBox(height: 20),
              Text(
                'Suas informações de contato são privadas e não serão exibidas às outras pessoas  ',
                style: TextStyle(color: NHColors.whiteColor.withOpacity(0.5)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
