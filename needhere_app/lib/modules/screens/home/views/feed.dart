import 'package:cached_network_image/cached_network_image.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:needhere_app/app/helpers/widgets/progress_indicator.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/post/presentation/states/post/cubit.dart';
import 'package:needhere_app/modules/post/presentation/states/post/state.dart';
import 'package:needhere_app/modules/screens/profile/screen.dart';
import 'package:skeletons/skeletons.dart';

class FeedView extends StatefulWidget {
  const FeedView({Key? key}) : super(key: key);

  @override
  FeedViewState createState() => FeedViewState();
}

class FeedViewState extends State<FeedView> {
  PageController? pagePublicationController;
  PageController? pagePhotoController;

  int _selectedPagePhotoIndex = 0;

  var faker = Faker();

  @override
  void initState() {
    super.initState();
    context.read<PostCubit>().getPosts();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void previousStepPhoto(int index) async {
    if (index > 0) {
      setState(
        () {
          _selectedPagePhotoIndex--;
        },
      );

      await pagePhotoController?.animateToPage(
        _selectedPagePhotoIndex,
        duration: const Duration(seconds: 5),
        curve: Curves.easeOut,
      );
    } else {
      debugPrint('start');
    }
  }

  void nextStepPhoto(int index) async {
    if (index < 2) {
      setState(
        () {
          _selectedPagePhotoIndex++;
        },
      );

      await pagePhotoController?.animateToPage(
        _selectedPagePhotoIndex,
        duration: const Duration(seconds: 5),
        curve: Curves.easeOut,
      );
    } else {
      debugPrint('end');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PostCubit, PostState>(
          // listener: (context, state) {},
          builder: (context, state) {
        if (state is PostingState) {
          return const SkeletonPosts();
        } else if (state is PostGettedState) {
          var _posts = state.posts;

          return PageView(
            controller: pagePublicationController,
            children: [
              for (var i = 0; i < _posts.length; i++)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Expanded(
                        flex: 6,
                        child: Row(
                          children: [
                            const RotatedBox(
                              quarterTurns: 3,
                              child: Text(
                                '30 minutos atrás',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: NHColors.whiteColor,
                                ),
                              ),
                            ),

                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  top: 24,
                                  bottom: 8,
                                ),
                                child: Stack(
                                  clipBehavior: Clip.none,
                                  children: [
                                    Stack(
                                      children: [
                                        _posts[i].pictures!.isEmpty
                                            ? Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                height: MediaQuery.of(context)
                                                    .size
                                                    .height,
                                                color: Colors.white12,
                                                child: Opacity(
                                                  opacity: 0.4,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: const [
                                                      Icon(
                                                        LineIcons.image,
                                                      ),
                                                      Text('Sem imagens')
                                                    ],
                                                  ),
                                                ),
                                              )
                                            : PageView(
                                                scrollDirection: Axis.vertical,
                                                physics:
                                                    const NeverScrollableScrollPhysics(),
                                                controller: pagePhotoController,
                                                children: [
                                                  for (var j = 0;
                                                      j <
                                                          _posts[i]
                                                              .pictures!
                                                              .length;
                                                      j++)
                                                    Column(
                                                      children: [
                                                        Expanded(
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl: _posts[i]
                                                                .pictures![j]
                                                                .toString(),
                                                            fit: BoxFit.fill,
                                                            placeholder:
                                                                (context,
                                                                        url) =>
                                                                    const Center(
                                                              child:
                                                                  NHProgressIndicator(
                                                                color: NHColors
                                                                    .secondaryColor,
                                                              ),
                                                            ),
                                                            errorWidget: (context,
                                                                    url,
                                                                    error) =>
                                                                const Icon(Icons
                                                                    .error),
                                                          ),

                                                          //     Container(
                                                          //   // decoration:
                                                          //   //     BoxDecoration(
                                                          //   //   image:
                                                          //   //       DecorationImage(
                                                          //   //     image: NetworkImage(
                                                          //   //         posts[i].pictures![
                                                          //   //             j],
                                                          //   //         scale:
                                                          //   //             1),
                                                          //   //     fit: BoxFit
                                                          //   //         .fitHeight,
                                                          //   //     colorFilter: ColorFilter.mode(
                                                          //   //         _selectedPagePhotoIndex == 0
                                                          //   //             ? Colors.red
                                                          //   //             : _selectedPagePhotoIndex == 1
                                                          //   //                 ? Colors.green
                                                          //   //                 : Colors.blue,
                                                          //   //         BlendMode.color),
                                                          //   //   ),
                                                          //   // ),
                                                          //   child: Row(
                                                          //     children: [
                                                          //       Flexible(
                                                          //         flex: 2,
                                                          //         child:
                                                          //             InkWell(
                                                          //           onTap:
                                                          //               () {
                                                          //             debugPrint('previous image');
                                                          //             previousStepPhoto(_selectedPagePhotoIndex);
                                                          //           },
                                                          //           child: Container(
                                                          //               // color: Colors.red
                                                          //               //     .withOpacity(
                                                          //               //         0.4),
                                                          //               ),
                                                          //         ),
                                                          //       ),
                                                          //       SizedBox(
                                                          //         width:
                                                          //             50,
                                                          //       ),
                                                          //       // Flexible(
                                                          //       //   flex: 6,
                                                          //       //   child: Container(
                                                          //       //     child: Row(
                                                          //       //       children: [
                                                          //       //         Flexible(
                                                          //       //           child: InkWell(
                                                          //       //             onTap: () {
                                                          //       //               previousStepPublication(
                                                          //       //                   i);
                                                          //       //             },
                                                          //       //             child:
                                                          //       //                 Container(
                                                          //       //               color: Colors
                                                          //       //                   .blue
                                                          //       //                   .withOpacity(
                                                          //       //                       0.4),
                                                          //       //             ),
                                                          //       //           ),
                                                          //       //         ),
                                                          //       //         Flexible(
                                                          //       //           child: InkWell(
                                                          //       //             onTap: () {
                                                          //       //               nextStepPublication(
                                                          //       //                   i);
                                                          //       //             },
                                                          //       //             child:
                                                          //       //                 Container(
                                                          //       //               color: Colors
                                                          //       //                   .yellow
                                                          //       //                   .withOpacity(
                                                          //       //                       0.4),
                                                          //       //             ),
                                                          //       //           ),
                                                          //       //         )
                                                          //       //       ],
                                                          //       //     ),
                                                          //       //   ),
                                                          //       // ),
                                                          //       Flexible(
                                                          //         flex: 2,
                                                          //         child:
                                                          //             InkWell(
                                                          //           onTap:
                                                          //               () {
                                                          //             debugPrint('next image');
                                                          //             nextStepPhoto(_selectedPagePhotoIndex);
                                                          //           },
                                                          //           child: Container(
                                                          //               // color: Colors.green
                                                          //               //     .withOpacity(
                                                          //               //         0.4),
                                                          //               ),
                                                          //         ),
                                                          //       ),
                                                          //     ],
                                                          //   ),
                                                          // ),
                                                        ),
                                                      ],
                                                    )
                                                ],
                                              ),

                                        Positioned(
                                          bottom: 0,
                                          right: 0,
                                          left: 0,
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(14.0),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Image.asset(
                                                        'assets/icons/share.png',
                                                        height: 22,
                                                      ),
                                                      const SizedBox(width: 8),
                                                      Text(
                                                        '${(i + 1) * 100}',
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(height: 12),
                                                  Row(
                                                    children: [
                                                      Image.asset(
                                                        'assets/icons/like-interactions.png',
                                                        height: 22,
                                                      ),
                                                      const SizedBox(width: 8),
                                                      Text(
                                                        // '${(i + 1) * 100}',
                                                        _posts[i]
                                                            .likes
                                                            .toString(),
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(height: 12),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      GestureDetector(
                                                        onTap: () {
                                                          int lenghtComments =
                                                              5;

                                                          showModalBottomSheet(
                                                            backgroundColor:
                                                                Colors.black,
                                                            isScrollControlled:
                                                                true,
                                                            context: context,
                                                            builder: (context) {
                                                              return Column(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .min,
                                                                children: [
                                                                  Row(
                                                                    children: [
                                                                      IconButton(
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.pop(
                                                                              context);
                                                                        },
                                                                        icon: const Icon(
                                                                            LineIcons.arrowLeft),
                                                                      ),
                                                                      const Spacer(),
                                                                      const SizedBox(
                                                                        width:
                                                                            45,
                                                                        child:
                                                                            Divider(
                                                                          color:
                                                                              Colors.white,
                                                                          thickness:
                                                                              3,
                                                                        ),
                                                                      ),
                                                                      const Spacer(),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height: 350,
                                                                    child:
                                                                        SingleChildScrollView(
                                                                      child: ListView
                                                                          .builder(
                                                                        shrinkWrap:
                                                                            true,
                                                                        primary:
                                                                            false,
                                                                        itemCount:
                                                                            lenghtComments,
                                                                        itemBuilder:
                                                                            (context,
                                                                                index) {
                                                                          return ListTile(
                                                                            leading:
                                                                                ClipPolygon(
                                                                              sides: 8,
                                                                              rotate: 112.0,
                                                                              boxShadows: [
                                                                                PolygonBoxShadow(color: Colors.black, elevation: 1.0),
                                                                                PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
                                                                              ],
                                                                              child: Image.asset(
                                                                                'assets/images/slides/slide-image-2.png',
                                                                                fit: BoxFit.fitWidth,
                                                                              ),
                                                                            ),
                                                                            title:
                                                                                Row(
                                                                              children: [
                                                                                SizedBox(
                                                                                  height: 25,
                                                                                  child: Column(
                                                                                    children: const [
                                                                                      Text(
                                                                                        'afadinha',
                                                                                        style: TextStyle(
                                                                                          fontSize: 10,
                                                                                          fontWeight: FontWeight.w600,
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                const Spacer(),
                                                                                const Text(
                                                                                  '5 sem 5 curtidas',
                                                                                  style: TextStyle(
                                                                                    fontSize: 11,
                                                                                    fontWeight: FontWeight.w600,
                                                                                  ),
                                                                                ),
                                                                                const Spacer(),
                                                                                TextButton(
                                                                                  child: const Text(
                                                                                    'Responder',
                                                                                    style: TextStyle(
                                                                                      fontSize: 11,
                                                                                      fontWeight: FontWeight.w600,
                                                                                      color: Colors.white,
                                                                                    ),
                                                                                  ),
                                                                                  onPressed: () {},
                                                                                ),
                                                                                const SizedBox(width: 10)
                                                                              ],
                                                                            ),
                                                                            subtitle:
                                                                                Column(
                                                                              children: [
                                                                                const Text(
                                                                                  'este é um comentário de alguma publicação, veja a forma como, de fato ficarão posicionados, independente de ser um comentário grande ou não, podendo conter até 160 caracteres',
                                                                                  style: TextStyle(
                                                                                    fontSize: 12,
                                                                                    fontWeight: FontWeight.w500,
                                                                                    color: Colors.white,
                                                                                  ),
                                                                                ),
                                                                                if (lenghtComments > index + 1) const Divider(thickness: 1)
                                                                              ],
                                                                            ),
                                                                          );
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  const Divider(
                                                                      thickness:
                                                                          1),
                                                                  Column(
                                                                    children: [
                                                                      Padding(
                                                                        padding:
                                                                            const EdgeInsets.all(8.0),
                                                                        child:
                                                                            Row(
                                                                          children: [
                                                                            SizedBox(
                                                                              height: 50,
                                                                              child: ClipPolygon(
                                                                                sides: 8,
                                                                                rotate: 112.0,
                                                                                boxShadows: [
                                                                                  PolygonBoxShadow(color: Colors.black, elevation: 1.0),
                                                                                  PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
                                                                                ],
                                                                                child: Image.asset(
                                                                                  'assets/images/slides/slide-image-2.png',
                                                                                  fit: BoxFit.cover,
                                                                                ),
                                                                              ),
                                                                            ),
                                                                            Expanded(
                                                                              child: Row(
                                                                                children: [
                                                                                  Expanded(
                                                                                    child: Padding(
                                                                                      padding: const EdgeInsets.only(
                                                                                        left: 12.0,
                                                                                        right: 12.0,
                                                                                      ),
                                                                                      child: FormBuilderTextField(
                                                                                        name: 'comment',
                                                                                        decoration: const InputDecoration(
                                                                                          hintText: 'Comentar como carlosbraga_...',
                                                                                          hintStyle: TextStyle(
                                                                                            fontSize: 13,
                                                                                            color: NHColors.primaryGreyColor,
                                                                                          ),
                                                                                          border: UnderlineInputBorder(),
                                                                                          enabledBorder: UnderlineInputBorder(),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                  TextButton(
                                                                                    onPressed: () {},
                                                                                    child: const Text(
                                                                                      'Publicar',
                                                                                      style: TextStyle(
                                                                                        color: NHColors.secondaryColor,
                                                                                        fontSize: 12,
                                                                                      ),
                                                                                    ),
                                                                                  )
                                                                                ],
                                                                              ),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      )
                                                                    ],
                                                                  )
                                                                ],
                                                              );
                                                            },
                                                          );
                                                        },
                                                        child: Row(
                                                          children: [
                                                            Image.asset(
                                                              'assets/icons/comments.png',
                                                              height: 22,
                                                            ),
                                                            const SizedBox(
                                                                width: 8),
                                                            Text(
                                                              '${(i + 1) * 100}',
                                                              style:
                                                                  const TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) {
                                                              return const ProfileScreen(
                                                                profileType:
                                                                    'other',
                                                                data: [
                                                                  'A Fadinha',
                                                                  '@afadinha'
                                                                ],
                                                              );
                                                            }),
                                                          );
                                                        },
                                                        child: Row(
                                                          children: [
                                                            const Text(
                                                              'afadinha ',
                                                              style: TextStyle(
                                                                fontSize: 13,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                              ),
                                                            ),
                                                            Opacity(
                                                              opacity: 0.7,
                                                              child:
                                                                  Image.asset(
                                                                'assets/icons/verified.png',
                                                                height: 14,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 20,
                                                        child: ElevatedButton(
                                                          onPressed: () {},
                                                          style: ButtonStyle(
                                                            backgroundColor:
                                                                MaterialStateColor
                                                                    .resolveWith(
                                                                        (states) =>
                                                                            NHColors.secondaryColor),
                                                            shape: MaterialStateProperty
                                                                .all<
                                                                    RoundedRectangleBorder>(
                                                              RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                  1,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          child: const Text(
                                                            'Seguir',
                                                            style: TextStyle(
                                                                fontSize: 12),
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),

                                        // Positioned(
                                        //   bottom: 0,
                                        //   child: Container(
                                        //     color: Colors.red,
                                        //     width:
                                        //         MediaQuery.of(context).size.width,
                                        //     child: Text('a'),
                                        //   ),
                                        // )
                                      ],
                                    ),
                                    Positioned(
                                      top: -18,
                                      child: Row(
                                        children: [
                                          Opacity(
                                            opacity: 0.7,
                                            child: Image.asset(
                                              'assets/icons/share.png',
                                              height: 16,
                                            ),
                                          ),
                                          const SizedBox(width: 3),
                                          const Text(
                                            'zau.maiano ',
                                            style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          const Opacity(
                                            opacity: 0.8,
                                            child: Text(
                                              'needhezou ',
                                              style: TextStyle(fontSize: 12),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      top: -18,
                                      right: 0,
                                      child: Row(
                                        children: [
                                          for (var i = 0; i < 2; i++)
                                            const Padding(
                                              padding: EdgeInsets.only(
                                                left: 2.0,
                                                right: 2.0,
                                              ),
                                              child: CircleAvatar(
                                                backgroundColor: Colors.white,
                                                radius: 3,
                                              ),
                                            )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                            // Expanded(
                            //   child: Column(
                            //     children: [
                            //       Stack(
                            //         children: [
                            //           Column(
                            //             children: [
                            //               Padding(
                            //                 padding: const EdgeInsets.only(
                            //                   left: 2,
                            //                   bottom: 4.0,
                            //                   right: 10,
                            //                 ),
                            //                 child: Row(
                            //                   children: [
                            //                     Row(
                            //                       children: [
                            //                         Opacity(
                            //                           opacity: 0.7,
                            //                           child: Image.asset(
                            //                             'assets/icons/share.png',
                            //                             height: 16,
                            //                           ),
                            //                         ),
                            //                         SizedBox(width: 3),
                            //                         Text(
                            //                           'zau.maiano ',
                            //                           style: const TextStyle(
                            //                             fontSize: 12,
                            //                             fontWeight: FontWeight.w700,
                            //                           ),
                            //                         ),
                            //                         Opacity(
                            //                           opacity: 0.8,
                            //                           child: Text(
                            //                             'Needhezou ',
                            //                             style: const TextStyle(
                            //                                 fontSize: 12),
                            //                           ),
                            //                         ),
                            //                       ],
                            //                     ),
                            //                     Spacer(),
                            //                     InkWell(
                            //                       onTap: () {
                            //                         showBottomSheet(
                            //                           backgroundColor: Colors.black,
                            //                           context: context,
                            //                           builder: (context) {
                            //                             return Column(
                            //                               mainAxisSize:
                            //                                   MainAxisSize.min,
                            //                               children: [
                            //                                 Row(
                            //                                   children: [
                            //                                     IconButton(
                            //                                       onPressed: () {
                            //                                         Navigator.pop(
                            //                                             context);
                            //                                       },
                            //                                       icon: Icon(LineIcons
                            //                                           .arrowLeft),
                            //                                     ),
                            //                                     Spacer(),
                            //                                     SizedBox(
                            //                                       width: 45,
                            //                                       child: Divider(
                            //                                         color: Colors
                            //                                             .white,
                            //                                         thickness: 3,
                            //                                       ),
                            //                                     ),
                            //                                     Spacer(),
                            //                                   ],
                            //                                 ),
                            //                                 SizedBox(
                            //                                   width:
                            //                                       double.infinity,
                            //                                   child: Padding(
                            //                                     padding:
                            //                                         const EdgeInsets
                            //                                             .all(8.0),
                            //                                     child: Column(
                            //                                       crossAxisAlignment:
                            //                                           CrossAxisAlignment
                            //                                               .start,
                            //                                       mainAxisAlignment:
                            //                                           MainAxisAlignment
                            //                                               .start,
                            //                                       children: [
                            //                                         Row(
                            //                                           crossAxisAlignment:
                            //                                               CrossAxisAlignment
                            //                                                   .center,
                            //                                           mainAxisAlignment:
                            //                                               MainAxisAlignment
                            //                                                   .center,
                            //                                           children: [
                            //                                             UserActionButtonWidget(
                            //                                               icon: LineIcons
                            //                                                   .link,
                            //                                               label:
                            //                                                   'Link',
                            //                                               onTap:
                            //                                                   () {},
                            //                                             ),
                            //                                             SizedBox(
                            //                                                 width:
                            //                                                     50),
                            //                                             UserActionButtonWidget(
                            //                                               icon: LineIcons
                            //                                                   .share,
                            //                                               label:
                            //                                                   'Compartilhar',
                            //                                               onTap:
                            //                                                   () {},
                            //                                             ),
                            //                                             SizedBox(
                            //                                                 width:
                            //                                                     50),
                            //                                             UserActionButtonWidget(
                            //                                               icon: LineIcons
                            //                                                   .envelope,
                            //                                               label:
                            //                                                   'Denunciar',
                            //                                               onTap:
                            //                                                   () {},
                            //                                             )
                            //                                           ],
                            //                                         ),
                            //                                         MaterialButton(
                            //                                           onPressed:
                            //                                               () {},
                            //                                           child: Text(
                            //                                             'Ocultar',
                            //                                             style: TextStyle(
                            //                                                 fontSize:
                            //                                                     12),
                            //                                           ),
                            //                                         ),
                            //                                         MaterialButton(
                            //                                           onPressed:
                            //                                               () {},
                            //                                           child: Text(
                            //                                             'Deixar de seguir',
                            //                                             style: TextStyle(
                            //                                                 fontSize:
                            //                                                     12),
                            //                                           ),
                            //                                         )
                            //                                       ],
                            //                                     ),
                            //                                   ),
                            //                                 )
                            //                               ],
                            //                             );
                            //                           },
                            //                         );
                            //                       },
                            //                       child: Padding(
                            //                         padding:
                            //                             const EdgeInsets.all(4.0),
                            //                         child: Row(
                            //                           children: [
                            //                             for (var i = 0; i < 2; i++)
                            //                               Padding(
                            //                                 padding:
                            //                                     const EdgeInsets
                            //                                         .only(
                            //                                   left: 2.0,
                            //                                   right: 2.0,
                            //                                 ),
                            //                                 child: CircleAvatar(
                            //                                   backgroundColor:
                            //                                       Colors.white,
                            //                                   radius: 3,
                            //                                 ),
                            //                               )
                            //                           ],
                            //                         ),
                            //                       ),
                            //                     ),
                            //                   ],
                            //                 ),
                            //               ),
                            //               Stack(
                            //                 clipBehavior: Clip.none,
                            //                 children: [
                            //                   Container(
                            //                     // height: 500,
                            //                     decoration: BoxDecoration(
                            //                       border: Border.all(
                            //                         width: 0.5,
                            //                         // color: NHColors.primaryGreyColor,
                            //                       ),
                            //                     ),
                            //                     child: Column(
                            //                       children: [
                            //                         SizedBox(
                            //                           height: MediaQuery.of(context)
                            //                                   .size
                            //                                   .height /
                            //                               2,
                            //                           child: PageView(
                            //                             scrollDirection:
                            //                                 Axis.vertical,
                            //                             physics:
                            //                                 const NeverScrollableScrollPhysics(),
                            //                             controller:
                            //                                 pagePhotoController,
                            //                             children: [
                            //                               for (var j = 0;
                            //                                   j < 3;
                            //                                   j++)
                            //                                 Column(
                            //                                   children: [
                            //                                     Container(
                            //                                       height: MediaQuery.of(
                            //                                                   context)
                            //                                               .size
                            //                                               .height /
                            //                                           2,
                            //                                       decoration:
                            //                                           const BoxDecoration(
                            //                                         image:
                            //                                             DecorationImage(
                            //                                           image:
                            //                                               AssetImage(
                            //                                             'assets/images/background-post.png',
                            //                                           ),
                            //                                           fit: BoxFit
                            //                                               .fill,
                            //                                         ),
                            //                                       ),
                            //                                       child: Column(
                            //                                         children: [
                            //                                           Flexible(
                            //                                             flex: 2,
                            //                                             child:
                            //                                                 InkWell(
                            //                                               onTap:
                            //                                                   () {
                            //                                                 debugPrint(
                            //                                                     'previous image');
                            //                                                 previousStepPhoto(
                            //                                                     j);
                            //                                               },
                            //                                               child:
                            //                                                   Container(),
                            //                                             ),
                            //                                           ),
                            //                                           Flexible(
                            //                                             flex: 6,
                            //                                             child:
                            //                                                 Container(
                            //                                               child:
                            //                                                   Row(
                            //                                                 children: [
                            //                                                   Flexible(
                            //                                                     child:
                            //                                                         InkWell(
                            //                                                       onTap: () {
                            //                                                         previousStepPublication(i);
                            //                                                       },
                            //                                                       child: Container(),
                            //                                                     ),
                            //                                                   ),
                            //                                                   Flexible(
                            //                                                     child:
                            //                                                         InkWell(
                            //                                                       onTap: () {
                            //                                                         nextStepPublication(i);
                            //                                                       },
                            //                                                       child: Container(),
                            //                                                     ),
                            //                                                   )
                            //                                                 ],
                            //                                               ),
                            //                                             ),
                            //                                           ),
                            //                                           Flexible(
                            //                                             flex: 2,
                            //                                             child:
                            //                                                 InkWell(
                            //                                               onTap:
                            //                                                   () {
                            //                                                 debugPrint(
                            //                                                     'next image');
                            //                                                 nextStepPhoto(
                            //                                                     j);
                            //                                               },
                            //                                               child:
                            //                                                   Container(),
                            //                                             ),
                            //                                           ),
                            //                                         ],
                            //                                       ),
                            //                                     ),
                            //                                   ],
                            //                                 )
                            //                             ],
                            //                           ),
                            //                         ),

                            //                         // Padding(
                            //                         //   padding:
                            //                         //       const EdgeInsets.only(
                            //                         //           left: 6,
                            //                         //           top: 6,
                            //                         //           right: 6,
                            //                         //           bottom: 6),
                            //                         //   child: Row(
                            //                         //     crossAxisAlignment:
                            //                         //         CrossAxisAlignment
                            //                         //             .center,
                            //                         //     mainAxisAlignment:
                            //                         //         MainAxisAlignment
                            //                         //             .center,
                            //                         //     children: [
                            //                         //       Expanded(
                            //                         //         child: Container(
                            //                         //           child: Row(
                            //                         //             children: [
                            //                         //               InkWell(
                            //                         //                 child: Row(
                            //                         //                   children: [
                            //                         //                     InkWell(
                            //                         //                       onTap:
                            //                         //                           () {
                            //                         //                         int lenghtComments =
                            //                         //                             5;

                            //                         //                         showModalBottomSheet(
                            //                         //                           backgroundColor:
                            //                         //                               Colors.black,
                            //                         //                           isScrollControlled:
                            //                         //                               true,
                            //                         //                           context:
                            //                         //                               context,
                            //                         //                           builder:
                            //                         //                               (context) {
                            //                         //                             return Column(
                            //                         //                               mainAxisSize: MainAxisSize.min,
                            //                         //                               children: [
                            //                         //                                 Row(
                            //                         //                                   children: [
                            //                         //                                     IconButton(
                            //                         //                                       onPressed: () {
                            //                         //                                         Navigator.pop(context);
                            //                         //                                       },
                            //                         //                                       icon: Icon(LineIcons.arrowLeft),
                            //                         //                                     ),
                            //                         //                                     Spacer(),
                            //                         //                                     SizedBox(
                            //                         //                                       width: 45,
                            //                         //                                       child: Divider(
                            //                         //                                         color: Colors.white,
                            //                         //                                         thickness: 3,
                            //                         //                                       ),
                            //                         //                                     ),
                            //                         //                                     Spacer(),
                            //                         //                                   ],
                            //                         //                                 ),
                            //                         //                                 Container(
                            //                         //                                   height: 350,
                            //                         //                                   child: SingleChildScrollView(
                            //                         //                                     child: ListView.builder(
                            //                         //                                       shrinkWrap: true,
                            //                         //                                       primary: false,
                            //                         //                                       itemCount: lenghtComments,
                            //                         //                                       itemBuilder: (context, index) {
                            //                         //                                         return ListTile(
                            //                         //                                           leading: ClipPolygon(
                            //                         //                                             sides: 8,
                            //                         //                                             rotate: 112.0,
                            //                         //                                             boxShadows: [
                            //                         //                                               PolygonBoxShadow(color: Colors.black, elevation: 1.0),
                            //                         //                                               PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
                            //                         //                                             ],
                            //                         //                                             child: Image.asset(
                            //                         //                                               'assets/images/slides/slide-image-2.png',
                            //                         //                                               fit: BoxFit.fitWidth,
                            //                         //                                             ),
                            //                         //                                           ),
                            //                         //                                           title: Container(
                            //                         //                                             child: Row(
                            //                         //                                               children: [
                            //                         //                                                 SizedBox(
                            //                         //                                                   height: 25,
                            //                         //                                                   child: Column(
                            //                         //                                                     children: [
                            //                         //                                                       Text(
                            //                         //                                                         'afadinha',
                            //                         //                                                         style: TextStyle(
                            //                         //                                                           fontSize: 10,
                            //                         //                                                           fontWeight: FontWeight.w600,
                            //                         //                                                         ),
                            //                         //                                                       ),
                            //                         //                                                     ],
                            //                         //                                                   ),
                            //                         //                                                 ),
                            //                         //                                                 Spacer(),
                            //                         //                                                 Text(
                            //                         //                                                   '5 sem 5 curtidas',
                            //                         //                                                   style: TextStyle(
                            //                         //                                                     fontSize: 11,
                            //                         //                                                     fontWeight: FontWeight.w600,
                            //                         //                                                   ),
                            //                         //                                                 ),
                            //                         //                                                 Spacer(),
                            //                         //                                                 TextButton(
                            //                         //                                                   child: Text(
                            //                         //                                                     'Responder',
                            //                         //                                                     style: TextStyle(
                            //                         //                                                       fontSize: 11,
                            //                         //                                                       fontWeight: FontWeight.w600,
                            //                         //                                                       color: Colors.white,
                            //                         //                                                     ),
                            //                         //                                                   ),
                            //                         //                                                   onPressed: () {},
                            //                         //                                                 ),
                            //                         //                                                 SizedBox(width: 10)
                            //                         //                                               ],
                            //                         //                                             ),
                            //                         //                                           ),
                            //                         //                                           subtitle: Column(
                            //                         //                                             children: [
                            //                         //                                               Text(
                            //                         //                                                 'este é um comentário de alguma publicação, veja a forma como, de fato ficarão posicionados, independente de ser um comentário grande ou não, podendo conter até 160 caracteres',
                            //                         //                                                 style: TextStyle(
                            //                         //                                                   fontSize: 12,
                            //                         //                                                   fontWeight: FontWeight.w500,
                            //                         //                                                   color: Colors.white,
                            //                         //                                                 ),
                            //                         //                                               ),
                            //                         //                                               if (lenghtComments > index + 1) Divider(thickness: 1)
                            //                         //                                             ],
                            //                         //                                           ),
                            //                         //                                         );
                            //                         //                                       },
                            //                         //                                     ),
                            //                         //                                   ),
                            //                         //                                 ),
                            //                         //                                 Divider(thickness: 1),
                            //                         //                                 Column(
                            //                         //                                   children: [
                            //                         //                                     Padding(
                            //                         //                                       padding: const EdgeInsets.all(8.0),
                            //                         //                                       child: Row(
                            //                         //                                         children: [
                            //                         //                                           SizedBox(
                            //                         //                                             height: 50,
                            //                         //                                             child: ClipPolygon(
                            //                         //                                               sides: 8,
                            //                         //                                               rotate: 112.0,
                            //                         //                                               boxShadows: [
                            //                         //                                                 PolygonBoxShadow(color: Colors.black, elevation: 1.0),
                            //                         //                                                 PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
                            //                         //                                               ],
                            //                         //                                               child: Image.asset(
                            //                         //                                                 'assets/images/slides/slide-image-2.png',
                            //                         //                                                 fit: BoxFit.cover,
                            //                         //                                               ),
                            //                         //                                             ),
                            //                         //                                           ),
                            //                         //                                           Expanded(
                            //                         //                                             child: Row(
                            //                         //                                               children: [
                            //                         //                                                 Expanded(
                            //                         //                                                   child: Padding(
                            //                         //                                                     padding: const EdgeInsets.only(
                            //                         //                                                       left: 12.0,
                            //                         //                                                       right: 12.0,
                            //                         //                                                     ),
                            //                         //                                                     child: FormBuilderTextField(
                            //                         //                                                       name: 'comment',
                            //                         //                                                       decoration: InputDecoration(
                            //                         //                                                         hintText: 'Comentar como carlosbraga_...',
                            //                         //                                                         hintStyle: TextStyle(
                            //                         //                                                           fontSize: 13,
                            //                         //                                                           color: NHColors.primaryGreyColor,
                            //                         //                                                         ),
                            //                         //                                                         border: UnderlineInputBorder(),
                            //                         //                                                         enabledBorder: UnderlineInputBorder(),
                            //                         //                                                       ),
                            //                         //                                                     ),
                            //                         //                                                   ),
                            //                         //                                                 ),
                            //                         //                                                 TextButton(
                            //                         //                                                   onPressed: () {},
                            //                         //                                                   child: Text(
                            //                         //                                                     'Publicar',
                            //                         //                                                     style: TextStyle(
                            //                         //                                                       color: NHColors.secondaryColor,
                            //                         //                                                       fontSize: 12,
                            //                         //                                                     ),
                            //                         //                                                   ),
                            //                         //                                                 )
                            //                         //                                               ],
                            //                         //                                             ),
                            //                         //                                           )
                            //                         //                                         ],
                            //                         //                                       ),
                            //                         //                                     )
                            //                         //                                   ],
                            //                         //                                 )
                            //                         //                               ],
                            //                         //                             );
                            //                         //                           },
                            //                         //                         );
                            //                         //                       },
                            //                         //                       child:
                            //                         //                           Row(
                            //                         //                         children: [
                            //                         //                           Text(
                            //                         //                             '${(i + 1) * 100}',
                            //                         //                             style:
                            //                         //                                 TextStyle(
                            //                         //                               fontSize: 10,
                            //                         //                               fontWeight: FontWeight.w600,
                            //                         //                             ),
                            //                         //                           ),
                            //                         //                           const SizedBox(
                            //                         //                               width: 5),
                            //                         //                           Image
                            //                         //                               .asset(
                            //                         //                             'assets/icons/comments.png',
                            //                         //                             height:
                            //                         //                                 22,
                            //                         //                           ),
                            //                         //                         ],
                            //                         //                       ),
                            //                         //                     ),
                            //                         //                     const SizedBox(
                            //                         //                         width:
                            //                         //                             10),
                            //                         //                     Row(
                            //                         //                       children: [
                            //                         //                         Text(
                            //                         //                           '${(i + 1) * 100}',
                            //                         //                           style:
                            //                         //                               TextStyle(
                            //                         //                             fontSize:
                            //                         //                                 10,
                            //                         //                             fontWeight:
                            //                         //                                 FontWeight.w600,
                            //                         //                           ),
                            //                         //                         ),
                            //                         //                         const SizedBox(
                            //                         //                             width:
                            //                         //                                 2),
                            //                         //                         Image
                            //                         //                             .asset(
                            //                         //                           'assets/icons/like-interactions.png',
                            //                         //                           height:
                            //                         //                               22,
                            //                         //                         ),
                            //                         //                       ],
                            //                         //                     ),
                            //                         //                   ],
                            //                         //                 ),
                            //                         //               ),
                            //                         //               Spacer(),
                            //                         //               Row(
                            //                         //                 children: [
                            //                         //                   InkWell(
                            //                         //                     onTap: () {
                            //                         //                       Navigator
                            //                         //                           .push(
                            //                         //                         context,
                            //                         //                         MaterialPageRoute(builder:
                            //                         //                             (context) {
                            //                         //                           return const ProfileScreen(
                            //                         //                             profileType:
                            //                         //                                 'other',
                            //                         //                             data: [
                            //                         //                               'A Fadinha',
                            //                         //                               '@afadinha'
                            //                         //                             ],
                            //                         //                           );
                            //                         //                         }),
                            //                         //                       );
                            //                         //                     },
                            //                         //                     child: Text(
                            //                         //                       'afadinha ',
                            //                         //                       style:
                            //                         //                           const TextStyle(
                            //                         //                         fontSize:
                            //                         //                             13,
                            //                         //                         fontWeight:
                            //                         //                             FontWeight.w600,
                            //                         //                       ),
                            //                         //                     ),
                            //                         //                   ),
                            //                         //                   Opacity(
                            //                         //                     opacity:
                            //                         //                         0.7,
                            //                         //                     child: Image
                            //                         //                         .asset(
                            //                         //                       'assets/icons/verified.png',
                            //                         //                       height:
                            //                         //                           20,
                            //                         //                     ),
                            //                         //                   )
                            //                         //                 ],
                            //                         //               ),
                            //                         //               Spacer(),
                            //                         //               Row(
                            //                         //                 children: [
                            //                         //                   Text(
                            //                         //                     '${(i + 1) * 100}',
                            //                         //                     style:
                            //                         //                         TextStyle(
                            //                         //                       fontSize:
                            //                         //                           10,
                            //                         //                       fontWeight:
                            //                         //                           FontWeight
                            //                         //                               .w600,
                            //                         //                     ),
                            //                         //                   ),
                            //                         //                   const SizedBox(
                            //                         //                       width: 5),
                            //                         //                   Image.asset(
                            //                         //                     'assets/icons/share.png',
                            //                         //                     height: 22,
                            //                         //                   ),
                            //                         //                   const SizedBox(
                            //                         //                       width: 5),
                            //                         //                   SizedBox(
                            //                         //                     height: 22,
                            //                         //                     child:
                            //                         //                         ElevatedButton(
                            //                         //                       onPressed:
                            //                         //                           () {},
                            //                         //                       child:
                            //                         //                           const Text(
                            //                         //                         'Seguir',
                            //                         //                         style: TextStyle(
                            //                         //                             fontSize:
                            //                         //                                 11),
                            //                         //                       ),
                            //                         //                       style:
                            //                         //                           ButtonStyle(
                            //                         //                         backgroundColor:
                            //                         //                             MaterialStateColor.resolveWith((states) =>
                            //                         //                                 NHColors.secondaryColor),
                            //                         //                         shape: MaterialStateProperty.all<
                            //                         //                             RoundedRectangleBorder>(
                            //                         //                           RoundedRectangleBorder(
                            //                         //                             borderRadius:
                            //                         //                                 BorderRadius.circular(
                            //                         //                               1,
                            //                         //                             ),
                            //                         //                           ),
                            //                         //                         ),
                            //                         //                       ),
                            //                         //                     ),
                            //                         //                   )
                            //                         //                 ],
                            //                         //               ),
                            //                         //             ],
                            //                         //           ),
                            //                         //         ),
                            //                         //       ),
                            //                         //     ],
                            //                         //   ),
                            //                         // ),
                            //                       ],
                            //                     ),
                            //                   ),
                            //                 ],
                            //               )
                            //             ],
                            //           ),
                            //           Positioned(
                            //             top: -5,
                            //             right: 50,
                            //             left: 50,
                            //             child: Align(
                            //               alignment: Alignment.center,
                            //               child: Container(
                            //                 color: Colors.transparent,
                            //                 height: 70,
                            //                 child: ClipPolygon(
                            //                   sides: 8,
                            //                   rotate: 112.0,
                            //                   boxShadows: [
                            //                     PolygonBoxShadow(
                            //                         color: Colors.black,
                            //                         elevation: 1.0),
                            //                     PolygonBoxShadow(
                            //                         color: Colors.grey,
                            //                         elevation: 5.0)
                            //                   ],
                            //                   child: Image.asset(
                            //                     'assets/images/slides/slide-image-2.png',
                            //                     fit: BoxFit.fitWidth,
                            //                   ),
                            //                 ),
                            //               ),
                            //             ),
                            //           )
                            //         ],
                            //       ),
                            //     ],
                            //   ),
                            // ),

                            if (_posts[i].pictures!.length > 1)
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  // Text(posts[i].pictures!.length.toString()),
                                  for (var k = 0;
                                      k < _posts[i].pictures!.length.toInt();
                                      k++)
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 4,
                                        top: 1.0,
                                        right: 4,
                                        bottom: 1.0,
                                      ),
                                      child: CircleAvatar(
                                        radius: 3,
                                        backgroundColor:
                                            i == _selectedPagePhotoIndex
                                                ? NHColors.secondaryColor
                                                    .withOpacity(0.7)
                                                : NHColors.primaryGreyColor,
                                      ),
                                    ),
                                ],
                              ),
                          ],
                        ),
                      ),

                      Expanded(
                          child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Text(
                                // faker.lorem.words((i + 1) * 30).toString(),
                                _posts[i].description,
                                style: const TextStyle(fontSize: 14),
                              )
                            ],
                          ),
                        ),
                      ))

                      // Flexible(
                      //   child: SingleChildScrollView(
                      //     child: Padding(
                      //       padding: const EdgeInsets.all(16.0),
                      //       child: Column(
                      //         children: [
                      //           Text(
                      //             faker.lorem.words((i + 1) * 30).toString(),
                      //             style: const TextStyle(fontSize: 14),
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                ),
            ],
          );
        } else {
          return Container();
        }
      }),
    );
  }
}

class SkeletonPosts extends StatelessWidget {
  const SkeletonPosts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Skeleton(
      isLoading: true,
      skeleton: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SizedBox(
            width: double.infinity,
            // color: Colors.red,
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: 20,
                      child: Container(color: Colors.white),
                    )),
                    const Spacer(),
                    // Expanded(child: Container()),
                    Row(
                      children: [
                        for (var i = 0; i < 2; i++)
                          const Padding(
                            padding: EdgeInsets.only(right: 3.0),
                            child: CircleAvatar(
                              radius: 4,
                            ),
                          )
                      ],
                    )
                  ],
                ),
                const SizedBox(height: 6),
                Expanded(
                  flex: 9,
                  child: Container(
                    color: Colors.white,
                  ),
                ),
                const SizedBox(height: 6),
                Expanded(
                  flex: 1,
                  child: Container(color: Colors.white),
                ),
              ],
            )),
      ),
      child: const Center(
        child: Text("Content"),
      ),
    );
  }
}
