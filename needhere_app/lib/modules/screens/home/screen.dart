import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:line_icons/line_icons.dart';
import 'package:needhere_app/app/helpers/widgets/user_builder.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/config/screen.dart';
import 'package:needhere_app/modules/screens/discovery/screen.dart';
import 'package:needhere_app/modules/screens/profile/screen.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:needhere_app/modules/screens/splash/screen.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/cubit.dart';
import 'package:needhere_app/modules/user/presentation/cubit/validate/cubit.dart';

import '../../user/presentation/cubit/logout/cubit.dart';
import 'views/feed.dart';

import 'dart:math' as math;

class HomeScreen extends StatefulWidget {
  static String route = '/home';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

enum _PicturePopupOption { Gallery, Camera }

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  PageController? _pageController;
  int _selectedPage = 0;

  @override
  void initState() {
    super.initState();

    _pageController = PageController(
      initialPage: 0,
    );

    // ref.read(validateDrawerNotifierProvider.notifier).validate();
    // context.read<ValidateCubit>().validate();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController!.dispose();

    // ref.read(validateDrawerNotifierProvider.notifier).dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.black,
        drawer: Theme(
          data: Theme.of(context).copyWith(canvasColor: Colors.black26),
          child: UserBuilder(builder: (context, user) {
            return Drawer(
              elevation: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 22.0, top: 60),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      user?.name ?? 'Sem nome',
                      style: const TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      '@${user?.userName}',
                      style: TextStyle(
                          fontSize: 15,
                          color: NHColors.whiteColor.withOpacity(0.6)),
                    ),
                    const SizedBox(height: 10),
                    InkWell(
                      onTap: () {
                        _scaffoldKey.currentState!.openEndDrawer();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return const ProfileScreen(
                                profileType: 'mine',
                                data: ['Carlos Braga', '@carlosbraaga_'],
                              );
                            },
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 8,
                          right: 8,
                          bottom: 8,
                        ),
                        child: Text(
                          AppLocalizations.of(context)!.profile,
                          style: TextStyle(
                            fontSize: 15,
                            color: NHColors.whiteColor.withOpacity(0.6),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return const ConfigurationScreen();
                          }),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 8,
                          right: 8,
                          bottom: 8,
                        ),
                        child: Text(
                          AppLocalizations.of(context)!.settings,
                          style: TextStyle(
                            fontSize: 15,
                            color: NHColors.whiteColor.withOpacity(0.6),
                          ),
                        ),
                      ),
                    ),
                    // InkWell(
                    //   onTap: () {},
                    //   child: Padding(
                    //     padding: const EdgeInsets.only(
                    //       top: 8,
                    //       right: 8,
                    //       bottom: 8,
                    //     ),
                    //     child: Text(
                    //       AppLocalizations.of(context)!.help,
                    //       style: TextStyle(
                    //         fontSize: 15,
                    //         color: NHColors.whiteColor.withOpacity(0.6),
                    //       ),
                    //     ),
                    //   ),
                    // ),

                    InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text(AppLocalizations.of(context)!.logout),
                              content: Text(
                                  AppLocalizations.of(context)!.logoutQuestion),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(AppLocalizations.of(context)!.no),
                                ),
                                TextButton(
                                  onPressed: () {
                                    // ref
                                    //     .read(authenticateNotifierProvider
                                    //         .notifier)
                                    //     .logout();

                                    context.read<LogoutCubit>().logout();

                                    Navigator.pushReplacementNamed(
                                      context,
                                      SplashScreen.route,
                                    );
                                  },
                                  child:
                                      Text(AppLocalizations.of(context)!.yes),
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 8,
                          right: 8,
                          bottom: 8,
                        ),
                        child: Text(
                          AppLocalizations.of(context)!.logout,
                          style: TextStyle(
                            fontSize: 15,
                            color: NHColors.whiteColor.withOpacity(0.6),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
        appBar: AppBar(
          backgroundColor: const Color(0xFF212121),
          elevation: 0,
          leading: Padding(
            padding: const EdgeInsets.only(left: 6.0),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    _scaffoldKey.currentState!.openDrawer();
                  },
                  child: Image.asset(
                    'assets/icons/menu.png',
                    height: 17,
                  ),
                ),
              ],
            ),
          ),
          title: Card(
            elevation: 0,
            color: const Color(0xFF262626),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(100)),
            child: Padding(
              padding: const EdgeInsets.only(
                left: 27.43,
                top: 2,
                right: 27.43,
                bottom: 2,
              ),
              child: CircleAvatar(
                backgroundColor: NHColors.secondaryColor.withOpacity(0.2),
                child: Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: CircleAvatar(
                    backgroundColor: const Color(0xFF262626),
                    child: Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: Image.asset(
                        'assets/images/logo.png',
                        height: 20,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: Image.asset(
                'assets/icons/like-interactions.png',
                height: 22,
              ),
              onPressed: () {},
            )
          ],
        ),
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: const [
            FeedView(),
            DiscoveryScreen(),
          ],
        ),
        bottomNavigationBar: Container(
          height: 38,
          color: NHColors.primaryBlackDetailColor,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.center,
                child: InkWell(
                  onTap: () async {
                    setState(() {
                      _selectedPage = 0;
                    });

                    await _pageController?.animateToPage(
                      _selectedPage,
                      curve: Curves.easeInOut,
                      duration: const Duration(milliseconds: 100),
                    );
                    // }
                  },
                  customBorder: const CircleBorder(),
                  child: Card(
                    color: NHColors.primaryBlackDetailColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    child: Image.asset(
                      'assets/icons/globe.png',
                      height: 28,
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 50),
              InkWell(
                onTap: () async {
                  showModalBottomSheet(
                    context: context,
                    backgroundColor: Colors.black,
                    isScrollControlled: true,
                    builder: (context) => Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              icon: const Icon(LineIcons.arrowLeft),
                            ),
                            const Text('Nova publicação'),
                          ],
                        ),
                        Container(
                          color: NHColors.primaryBlackDetailColor,
                          width: MediaQuery.of(context).size.width,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              customBorder: const CircleBorder(),
                              child: Transform.rotate(
                                angle: -math.pi / 4,
                                child: Image.asset(
                                  'assets/icons/more.png',
                                  height: 27,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
                customBorder: const CircleBorder(),
                child: Card(
                  color: NHColors.primaryBlackDetailColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100)),
                  child: Image.asset(
                    'assets/icons/more.png',
                    height: 28,
                  ),
                ),
              ),
              const SizedBox(width: 50),
              Align(
                alignment: Alignment.center,
                child: InkWell(
                  onTap: () async {
                    setState(() {
                      _selectedPage = 1;
                    });

                    await _pageController?.animateToPage(
                      _selectedPage,
                      curve: Curves.easeInOut,
                      duration: const Duration(milliseconds: 100),
                    );
                  },
                  customBorder: const CircleBorder(),
                  child: Card(
                    color: NHColors.primaryBlackDetailColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    child: Image.asset(
                      'assets/icons/discover.png',
                      height: 27,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UserActionButtonWidget extends StatelessWidget {
  final IconData icon;
  final String label;
  final Function onTap;

  const UserActionButtonWidget({
    Key? key,
    required this.icon,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Column(
        children: [
          Card(
            shape: const CircleBorder(),
            color: NHColors.primaryBlackDetailColor.withOpacity(0.4),
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Icon(
                icon,
                size: 25,
              ),
            ),
          ),
          Text(
            label,
            style: const TextStyle(
              fontSize: 11,
            ),
          )
        ],
      ),
    );
  }
}
