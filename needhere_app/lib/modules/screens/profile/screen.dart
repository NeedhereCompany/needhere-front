import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/model/image_model.dart';
import 'package:needhere_app/modules/screens/discovery/screen.dart';
import 'package:storage_path/storage_path.dart';
import 'package:transparent_image/transparent_image.dart';

class ProfileScreen extends StatefulWidget {
  final String profileType;
  final dynamic data;

  const ProfileScreen({
    Key? key,
    required this.profileType,
    this.data,
  }) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isChecked = false;

  TextEditingController textEditingController = TextEditingController();

  int _selectedPage = 0;
  PageController? _pageController;

  @override
  void initState() {
    _pageController = PageController(
      initialPage: 0,
      keepPage: true,
    );

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryColor,
        body: Builder(builder: (context) {
          return PageView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _pageController,
            children: [
              Builder(builder: (context) {
                return CustomScrollView(
                  physics: const NeverScrollableScrollPhysics(),
                  slivers: [
                    SliverAppBar(
                      expandedHeight: 175,
                      elevation: 0,
                      excludeHeaderSemantics: true,
                      // automaticallyImplyLeading: false,
                      leading: SizedBox(
                        height: 10,
                        child: Opacity(
                          opacity: 0.6,
                          child: Card(
                            color: Colors.white12,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: const Icon(Icons.arrow_back, size: 22),
                            ),
                          ),
                        ),
                      ),

                      pinned: true,
                      floating: true,
                      backgroundColor: Colors.black,
                      flexibleSpace: FlexibleSpaceBar(
                        background: Stack(
                          // overflow: Overflow.visible,
                          // alignment: Alignment.center,
                          children: [
                            Image.asset(
                              'assets/images/slides/main.png',
                              fit: BoxFit.fill,
                              height: 140,
                              width: MediaQuery.of(context).size.width,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                    isScrollControlled: true,
                                    context: context,
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        topRight: Radius.circular(20),
                                      ),
                                    ),
                                    backgroundColor: NHColors.primaryColor,
                                    builder: (context) {
                                      return SafeArea(
                                        child: SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              1.1,
                                          child: Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    const Text(
                                                      'Foto',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    const Spacer(),
                                                    InkWell(
                                                      customBorder:
                                                          const CircleBorder(),
                                                      onTap: () {},
                                                      child: Card(
                                                        elevation: 0,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      100),
                                                        ),
                                                        color:
                                                            Colors.transparent,
                                                        child: const Icon(
                                                          Icons.arrow_right_alt,
                                                          size: 20,
                                                          color: NHColors
                                                              .secondaryColor,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                FutureBuilder(
                                                  future:
                                                      StoragePath.imagesPath,
                                                  builder: (BuildContext
                                                          context,
                                                      AsyncSnapshot snapshot) {
                                                    if (snapshot.hasData) {
                                                      List<dynamic> list =
                                                          json.decode(
                                                              snapshot.data);

                                                      return SizedBox(
                                                        height: 300,
                                                        child: GridView.builder(
                                                          gridDelegate:
                                                              const SliverGridDelegateWithFixedCrossAxisCount(
                                                            crossAxisCount: 2,
                                                          ),
                                                          itemCount:
                                                              list.length,
                                                          itemBuilder:
                                                              (BuildContext
                                                                      context,
                                                                  int index) {
                                                            ImageModel
                                                                imageModel =
                                                                ImageModel
                                                                    .fromJson(list[
                                                                        index]);
                                                            return Stack(
                                                              alignment: Alignment
                                                                  .bottomCenter,
                                                              children: <
                                                                  Widget>[
                                                                FadeInImage(
                                                                  image:
                                                                      FileImage(
                                                                    File(imageModel
                                                                        .files![0]),
                                                                  ),
                                                                  // placeholder: MemoryImage(kTransparentImage),
                                                                  fit: BoxFit
                                                                      .cover,
                                                                  width: double
                                                                      .infinity,
                                                                  height: double
                                                                      .infinity,
                                                                  placeholder:
                                                                      MemoryImage(
                                                                          kTransparentImage),
                                                                ),
                                                                Container(
                                                                  color: Colors
                                                                      .black
                                                                      .withOpacity(
                                                                          0.7),
                                                                  height: 30,
                                                                  width: double
                                                                      .infinity,
                                                                  child: Center(
                                                                    child: Text(
                                                                      imageModel
                                                                              .folderName ??
                                                                          "",
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: const TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              16,
                                                                          fontFamily:
                                                                              'Regular'),
                                                                    ),
                                                                  ),
                                                                )
                                                              ],
                                                            );
                                                          },
                                                        ),
                                                      );
                                                    } else {
                                                      return Container();
                                                    }
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                },
                                child: Positioned(
                                  top: 10,
                                  // right: 10,
                                  bottom: -80,
                                  child: SizedBox(
                                    height: 80,
                                    child: ClipPolygon(
                                      sides: 8,
                                      rotate: 112.0,
                                      boxShadows: [
                                        PolygonBoxShadow(
                                            color: Colors.black,
                                            elevation: 1.0),
                                        PolygonBoxShadow(
                                            color: Colors.grey, elevation: 5.0)
                                      ],
                                      child: Image.asset(
                                        'assets/images/slides/slide-image-2.png',
                                        fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                  ),

                                  // CircleAvatar(
                                  //   backgroundColor: Color(0xFF09B23F),
                                  //   radius: 40,
                                  //   child: CircleAvatar(
                                  //       radius: 39,
                                  //       // backgroundColor: Colors.red,
                                  //       backgroundImage: AssetImage(
                                  //         'assets/images/slides/main.png',
                                  //       )),
                                  // ),
                                ),
                              ),
                            )
                          ],
                        ),
                        // collapseMode: CollapseMode.parallax,
                        centerTitle: true,
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Stack(
                            alignment: Alignment.center,
                            clipBehavior: Clip.antiAlias,
                            children: [
                              Container(
                                height: MediaQuery.of(context).size.height,
                                color: Colors.black,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            height: 25,
                                            child: ElevatedButton(
                                              onPressed: () {},
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateColor
                                                        .resolveWith((states) =>
                                                            NHColors
                                                                .secondaryColor),
                                                shape:
                                                    MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      4,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              child: const Text(
                                                'Seguir',
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(width: 5),
                                          SizedBox(
                                            height: 25,
                                            width: 75,
                                            child: FormBuilderDropdown(
                                              name: 'options',
                                              items: const [],
                                              decoration: InputDecoration(
                                                isDense: true,
                                                isCollapsed: true,
                                                prefixIcon: Opacity(
                                                  opacity: widget.profileType
                                                          .contains('mine')
                                                      ? 0
                                                      : 0.6,
                                                  child: Image.asset(
                                                    'assets/icons/horse-ocean.png',
                                                  ),
                                                ),
                                                suffixIcon: const Icon(
                                                    Icons.arrow_drop_down),
                                                border:
                                                    const OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                  color: Colors.black,
                                                )),
                                                enabledBorder:
                                                    const OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                  color: Colors.black,
                                                )),
                                                focusedBorder:
                                                    const OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                  color: Colors.black,
                                                )),
                                              ),
                                            ),
                                          ),
                                          Column(
                                            children: [
                                              Text(
                                                // 'A Fadinha',
                                                widget.data[0].toString(),
                                                style: const TextStyle(
                                                    fontSize: 14),
                                              ),
                                              Text(
                                                // '@afadinha',
                                                widget.data[1].toString(),
                                                style: const TextStyle(
                                                    fontSize: 11),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(height: 8),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: const [
                                        Text(
                                          "120 Need's",
                                          style: TextStyle(
                                            color: Color(0xFF6F6F72),
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Text(
                                          "1.2M Needzer's",
                                          style: TextStyle(
                                            color: Color(0xFF6F6F72),
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Text(
                                          "1 mil seguindo",
                                          style: TextStyle(
                                            color: Color(0xFF6F6F72),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 15),
                                    const Padding(
                                      padding: EdgeInsets.only(
                                          left: 50.0, right: 50.0),
                                      child: Text(
                                        'Esta é a minha bio, estou bem feliz em estar junto com você neste mega projeto.',
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    // const SizedBox(height: 15),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          onTap: () {},
                                          child: Image.asset(
                                            'assets/icons/infinite.png',
                                            height: 60,
                                          ),
                                        ),
                                        const SizedBox(width: 40),
                                        InkWell(
                                          onTap: () {},
                                          child: Image.asset(
                                            'assets/icons/share.png',
                                            height: 25,
                                          ),
                                        ),
                                      ],
                                    ),
                                    // const SizedBox(height: 15),

                                    // Column(
                                    //   children: [
                                    //     Expanded(
                                    //       child: Padding(
                                    //         padding: const EdgeInsets.all(2.0),
                                    //         child: Image.asset(
                                    //             'assets/images/slides/main.png'),
                                    //       ),
                                    //     ),
                                    //     Expanded(
                                    //       child: SingleChildScrollView(
                                    //         scrollDirection: Axis.horizontal,
                                    //         child: Row(
                                    //           children: [
                                    //             for (var i = 0; i < 6; i++)
                                    //               Padding(
                                    //                 padding:
                                    //                     const EdgeInsets.all(2.0),
                                    //                 child: Image.asset(
                                    //                   'assets/images/slides/slide-image-${i + 1}.png',
                                    //                   fit: BoxFit.cover,
                                    //                 ),
                                    //               ),
                                    //           ],
                                    //         ),
                                    //       ),
                                    //     )
                                    //   ],
                                    // )

                                    Expanded(
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Image.asset(
                                                'assets/images/slides/main.png'),
                                          ),

                                          SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: Row(
                                              children: [
                                                for (var i = 0; i < 6; i++)
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            2.0),
                                                    child: Image.asset(
                                                      'assets/images/slides/slide-image-${i + 1}.png',
                                                      fit: BoxFit.fill,
                                                      height: 245,
                                                    ),
                                                  ),
                                              ],
                                            ),
                                          )
                                          // Container(
                                          //   child: Padding(
                                          //     padding:
                                          //         const EdgeInsets.all(2.0),
                                          //     child: Image.asset(
                                          //       'assets/images/slides/main.png',
                                          //       height: 80,
                                          //     ),
                                          //   ),
                                          // ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                );
              }),
              Container(),
              const DiscoveryScreen()
            ],
          );
        }),
        bottomNavigationBar: InkWell(
          onTap: () {
            showModalBottomSheet(
              context: context,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              backgroundColor: NHColors.primaryColor,
              builder: (context) {
                return SafeArea(
                  child: Wrap(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                IconButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  icon: const Icon(LineIcons.arrowLeft),
                                ),
                                const Spacer(),
                                const SizedBox(
                                  width: 45,
                                  child: Divider(
                                    color: Colors.white,
                                    thickness: 3,
                                  ),
                                ),
                                const Spacer(),
                              ],
                            ),
                            if (!widget.profileType.contains('mine'))
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                    isScrollControlled: true,
                                    context: context,
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        topRight: Radius.circular(20),
                                      ),
                                    ),
                                    backgroundColor: NHColors.primaryColor,
                                    builder: (context) {
                                      return SafeArea(
                                        child: SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              1.1,
                                          child: Padding(
                                            padding: const EdgeInsets.all(30.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    IconButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      icon: const Icon(
                                                          Icons.arrow_back),
                                                    ),
                                                    const Spacer(),
                                                    const SizedBox(
                                                      width: 45,
                                                      child: Divider(
                                                        color: Colors.white,
                                                        thickness: 3,
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                  ],
                                                ),
                                                const Center(
                                                  child: Text(
                                                    'Denunciar',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                const SizedBox(height: 20),
                                                const Text(
                                                  'Por que você está denunciando esta conta?',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                const SizedBox(height: 20),
                                                const Text(
                                                  "Sua denuncia é anônima, exceto se você estiver denunciando uma violação de propriedade intelectual. Se alguém estiver em perigo imediato, ligue para o serviço de emergência local.",
                                                  style: TextStyle(
                                                      color: Color(0xFFBCBCBA)),
                                                ),
                                                const SizedBox(height: 20),
                                                ListView(
                                                  shrinkWrap: true,
                                                  children: [
                                                    ListTile(
                                                      title: const Text(
                                                        'Denunciar publicação, mensagem ou comentário',
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        int value = 1;

                                                        showModalBottomSheet(
                                                          isScrollControlled:
                                                              true,
                                                          context: context,
                                                          shape:
                                                              const RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(20),
                                                            ),
                                                          ),
                                                          backgroundColor:
                                                              NHColors
                                                                  .primaryColor,
                                                          builder: (context) {
                                                            return SafeArea(
                                                              child: SizedBox(
                                                                height: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height /
                                                                    1.1,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .all(
                                                                          30.0),
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Row(
                                                                        children: [
                                                                          IconButton(
                                                                            onPressed:
                                                                                () {
                                                                              Navigator.pop(context);
                                                                            },
                                                                            icon:
                                                                                const Icon(Icons.arrow_back),
                                                                          ),
                                                                          const Spacer(),
                                                                          const SizedBox(
                                                                            width:
                                                                                45,
                                                                            child:
                                                                                Divider(
                                                                              color: Colors.white,
                                                                              thickness: 3,
                                                                            ),
                                                                          ),
                                                                          const Spacer(),
                                                                        ],
                                                                      ),
                                                                      const Center(
                                                                        child:
                                                                            Text(
                                                                          'Denunciar',
                                                                          style:
                                                                              TextStyle(fontWeight: FontWeight.bold),
                                                                        ),
                                                                      ),
                                                                      const SizedBox(
                                                                          height:
                                                                              20),
                                                                      const Text(
                                                                        'Por que você está denunciando esta conta?',
                                                                        style: TextStyle(
                                                                            fontWeight:
                                                                                FontWeight.bold),
                                                                      ),
                                                                      const SizedBox(
                                                                          height:
                                                                              20),
                                                                      const Text(
                                                                        "Sua denuncia é anônima, exceto se você estiver denunciando uma violação de propriedade intelectual. Se alguém estiver em perigo imediato, ligue para o serviço de emergência local.",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Color(0xFFBCBCBA)),
                                                                      ),
                                                                      const SizedBox(
                                                                          height:
                                                                              20),
                                                                      ListView(
                                                                        shrinkWrap:
                                                                            true,
                                                                        children: [
                                                                          ListTile(
                                                                            title:
                                                                                const Text(
                                                                              'Está publicando conteúdo que não deveria estar na Neddhere',
                                                                              style: TextStyle(
                                                                                fontSize: 14,
                                                                              ),
                                                                            ),
                                                                            onTap:
                                                                                () {},
                                                                            contentPadding:
                                                                                EdgeInsets.zero,
                                                                            dense:
                                                                                true,
                                                                          ),
                                                                          ListTile(
                                                                            title:
                                                                                const Text(
                                                                              'Está fingindo ser outra pessoa',
                                                                              style: TextStyle(
                                                                                fontSize: 14,
                                                                              ),
                                                                            ),
                                                                            onTap:
                                                                                () {
                                                                              int value = 1;

                                                                              showModalBottomSheet(
                                                                                isScrollControlled: true,
                                                                                context: context,
                                                                                shape: const RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.only(
                                                                                    topLeft: Radius.circular(20),
                                                                                    topRight: Radius.circular(20),
                                                                                  ),
                                                                                ),
                                                                                backgroundColor: NHColors.primaryColor,
                                                                                builder: (context) {
                                                                                  return StatefulBuilder(
                                                                                    builder: (BuildContext context, setState) {
                                                                                      return SafeArea(
                                                                                        child: SizedBox(
                                                                                          height: MediaQuery.of(context).size.height / 1.1,
                                                                                          child: Padding(
                                                                                            padding: const EdgeInsets.only(left: 30, top: 30.0, right: 30),
                                                                                            child: SingleChildScrollView(
                                                                                              child: Column(
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                children: [
                                                                                                  Row(
                                                                                                    children: [
                                                                                                      IconButton(
                                                                                                        onPressed: () {
                                                                                                          Navigator.pop(context);
                                                                                                        },
                                                                                                        icon: const Icon(Icons.arrow_back),
                                                                                                      ),
                                                                                                      const Spacer(),
                                                                                                      const SizedBox(
                                                                                                        width: 45,
                                                                                                        child: Divider(
                                                                                                          color: Colors.white,
                                                                                                          thickness: 3,
                                                                                                        ),
                                                                                                      ),
                                                                                                      const Spacer(),
                                                                                                    ],
                                                                                                  ),
                                                                                                  const Center(
                                                                                                    child: Text(
                                                                                                      'Denunciar',
                                                                                                      style: TextStyle(fontWeight: FontWeight.bold),
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(height: 20),
                                                                                                  const Text(
                                                                                                    'Que engraçadinho! Quem essa conta está fingindo ser?',
                                                                                                    style: TextStyle(fontWeight: FontWeight.bold),
                                                                                                  ),
                                                                                                  const SizedBox(height: 20),
                                                                                                  ListView(
                                                                                                    shrinkWrap: true,
                                                                                                    children: [
                                                                                                      ListTile(
                                                                                                        title: const Text(
                                                                                                          'Eu',
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 14,
                                                                                                          ),
                                                                                                        ),
                                                                                                        trailing: Radio(
                                                                                                          value: 0,
                                                                                                          groupValue: value,
                                                                                                          activeColor: Colors.white,
                                                                                                          onChanged: (int? value) {
                                                                                                            setState(() {
                                                                                                              value = value!;
                                                                                                            });
                                                                                                          },
                                                                                                        ),
                                                                                                        contentPadding: EdgeInsets.zero,
                                                                                                      ),
                                                                                                      ListTile(
                                                                                                        title: const Text(
                                                                                                          'Alguém que eu conheço',
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 14,
                                                                                                          ),
                                                                                                        ),
                                                                                                        trailing: Radio(
                                                                                                          value: 1,
                                                                                                          groupValue: value,
                                                                                                          activeColor: Colors.white,
                                                                                                          onChanged: (int? value) {
                                                                                                            setState(() {
                                                                                                              value = value!;
                                                                                                            });
                                                                                                          },
                                                                                                        ),
                                                                                                        contentPadding: EdgeInsets.zero,
                                                                                                      ),
                                                                                                      ListTile(
                                                                                                        title: const Text(
                                                                                                          'Uma celebridade ou figura pública',
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 14,
                                                                                                          ),
                                                                                                        ),
                                                                                                        trailing: Radio(
                                                                                                          value: 2,
                                                                                                          groupValue: value,
                                                                                                          activeColor: Colors.white,
                                                                                                          onChanged: (int? value) {
                                                                                                            setState(() {
                                                                                                              value = value!;
                                                                                                            });
                                                                                                          },
                                                                                                        ),
                                                                                                        contentPadding: EdgeInsets.zero,
                                                                                                      ),
                                                                                                      ListTile(
                                                                                                        title: const Text(
                                                                                                          'Uma empresa ou organização',
                                                                                                          style: TextStyle(
                                                                                                            fontSize: 14,
                                                                                                          ),
                                                                                                        ),
                                                                                                        trailing: Radio(
                                                                                                          value: 3,
                                                                                                          groupValue: value,
                                                                                                          activeColor: Colors.white,
                                                                                                          onChanged: (int? value) {
                                                                                                            setState(() {
                                                                                                              value = value!;
                                                                                                            });
                                                                                                          },
                                                                                                        ),
                                                                                                        contentPadding: EdgeInsets.zero,
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                  const SizedBox(height: 30),
                                                                                                  Center(
                                                                                                    child: Image.asset(
                                                                                                      'assets/icons/selected.png',
                                                                                                      height: 65,
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(height: 15),
                                                                                                  const Center(
                                                                                                    child: Text(
                                                                                                      'Agradecemos o aviso',
                                                                                                      style: TextStyle(
                                                                                                        fontSize: 18,
                                                                                                      ),
                                                                                                    ),
                                                                                                  ),
                                                                                                  const SizedBox(height: 30),
                                                                                                  const Text(
                                                                                                    "A Needhere é feita de pessoas para pessoas e seu feedback é muito importante para nos ajudar a manter o universo seguro.",
                                                                                                    style: TextStyle(color: Color(0xFFBCBCBA)),
                                                                                                  ),
                                                                                                  const SizedBox(height: 50),
                                                                                                  Padding(
                                                                                                    padding: const EdgeInsets.only(
                                                                                                      left: 80.0,
                                                                                                      right: 80.0,
                                                                                                    ),
                                                                                                    child: NHButton(
                                                                                                      title: 'Concluir',
                                                                                                      disabled: false,
                                                                                                      onTap: () {},
                                                                                                    ),
                                                                                                  )
                                                                                                ],
                                                                                              ),
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      );
                                                                                    },
                                                                                  );
                                                                                },
                                                                              );
                                                                            },
                                                                            contentPadding:
                                                                                EdgeInsets.zero,
                                                                            dense:
                                                                                true,
                                                                          ),
                                                                          ListTile(
                                                                            title:
                                                                                const Text(
                                                                              'Pode ser menor de 13 anos idade',
                                                                              style: TextStyle(
                                                                                fontSize: 14,
                                                                              ),
                                                                            ),
                                                                            onTap:
                                                                                () {
                                                                              showModalBottomSheet(
                                                                                isScrollControlled: true,
                                                                                context: context,
                                                                                shape: const RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.only(
                                                                                    topLeft: Radius.circular(20),
                                                                                    topRight: Radius.circular(20),
                                                                                  ),
                                                                                ),
                                                                                backgroundColor: NHColors.primaryColor,
                                                                                builder: (context) {
                                                                                  return StatefulBuilder(
                                                                                    builder: (BuildContext context, setState) {
                                                                                      return SafeArea(
                                                                                        child: SizedBox(
                                                                                          height: MediaQuery.of(context).size.height / 1.1,
                                                                                          child: Padding(
                                                                                            padding: const EdgeInsets.all(30.0),
                                                                                            child: Column(
                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                                              children: [
                                                                                                Row(
                                                                                                  children: [
                                                                                                    IconButton(
                                                                                                      onPressed: () {
                                                                                                        Navigator.pop(context);
                                                                                                      },
                                                                                                      icon: const Icon(Icons.arrow_back),
                                                                                                    ),
                                                                                                    const Spacer(),
                                                                                                    const SizedBox(
                                                                                                      width: 45,
                                                                                                      child: Divider(
                                                                                                        color: Colors.white,
                                                                                                        thickness: 3,
                                                                                                      ),
                                                                                                    ),
                                                                                                    const Spacer(),
                                                                                                  ],
                                                                                                ),
                                                                                                const Center(
                                                                                                  child: Text(
                                                                                                    'Denunciar',
                                                                                                    style: TextStyle(fontWeight: FontWeight.bold),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(height: 20),
                                                                                                const Text(
                                                                                                  'Sobre denunciar uma criança com menos de 13 anos',
                                                                                                  style: TextStyle(fontWeight: FontWeight.bold),
                                                                                                ),
                                                                                                const SizedBox(height: 20),
                                                                                                const Text(
                                                                                                  "A Needhere exige que todos tenham no mínimo 13 anos de idade para poderem criar uma conta. Em algumas jurisdições, esse limite de idade pode ser maior. \n\nContas que representem alguém menor de 13 anos devem declarar claramente na biografia que a conta é gerenciada pelo pai, mão ou responsável. Se seu filho for menor de 13 anos e tiver uma conta que não é gerenciada por você ou outro responsável, você pode mostrar a ele com Excluir a Conta.",
                                                                                                  style: TextStyle(color: Color(0xFFBCBCBA)),
                                                                                                ),
                                                                                                const SizedBox(height: 30),
                                                                                                Center(
                                                                                                  child: Image.asset(
                                                                                                    'assets/icons/selected.png',
                                                                                                    height: 65,
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(height: 20),
                                                                                                const Center(
                                                                                                  child: Text(
                                                                                                    'Agradecemos a compreensão',
                                                                                                    style: TextStyle(
                                                                                                      fontSize: 18,
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                const SizedBox(height: 30),
                                                                                                const Text(
                                                                                                  "A Needhere é feita de pessoas para pessoas e seu feedback é muito importante para nos ajudar a manter o universo seguro.",
                                                                                                  style: TextStyle(color: Color(0xFFBCBCBA)),
                                                                                                ),
                                                                                                const SizedBox(height: 50),
                                                                                                Padding(
                                                                                                  padding: const EdgeInsets.only(
                                                                                                    left: 80.0,
                                                                                                    right: 80.0,
                                                                                                  ),
                                                                                                  child: NHButton(
                                                                                                    title: 'Concluir',
                                                                                                    disabled: false,
                                                                                                    onTap: () {},
                                                                                                  ),
                                                                                                )
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      );
                                                                                    },
                                                                                  );
                                                                                },
                                                                              );
                                                                            },
                                                                            contentPadding:
                                                                                EdgeInsets.zero,
                                                                            dense:
                                                                                true,
                                                                          )
                                                                        ],
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            );
                                                          },
                                                        );
                                                      },
                                                      contentPadding:
                                                          EdgeInsets.zero,
                                                      dense: true,
                                                    ),
                                                    ListTile(
                                                      title: const Text(
                                                        'Denunciar conta',
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                      ),
                                                      onTap: () {
                                                        showModalBottomSheet(
                                                          isScrollControlled:
                                                              true,
                                                          context: context,
                                                          shape:
                                                              const RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(20),
                                                            ),
                                                          ),
                                                          backgroundColor:
                                                              NHColors
                                                                  .primaryColor,
                                                          builder: (context) {
                                                            return StatefulBuilder(
                                                              builder:
                                                                  (BuildContext
                                                                          context,
                                                                      setState) {
                                                                return SafeArea(
                                                                  child:
                                                                      SizedBox(
                                                                    height: MediaQuery.of(context)
                                                                            .size
                                                                            .height /
                                                                        1.1,
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                              .all(
                                                                          30.0),
                                                                      child:
                                                                          SingleChildScrollView(
                                                                        child:
                                                                            Column(
                                                                          mainAxisSize:
                                                                              MainAxisSize.min,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: [
                                                                            Row(
                                                                              children: [
                                                                                IconButton(
                                                                                  onPressed: () {
                                                                                    Navigator.pop(context);
                                                                                  },
                                                                                  icon: const Icon(Icons.arrow_back),
                                                                                ),
                                                                                const Spacer(),
                                                                                const SizedBox(
                                                                                  width: 45,
                                                                                  child: Divider(
                                                                                    color: Colors.white,
                                                                                    thickness: 3,
                                                                                  ),
                                                                                ),
                                                                                const Spacer(),
                                                                              ],
                                                                            ),
                                                                            const Center(
                                                                              child: Text(
                                                                                'Denunciar',
                                                                                style: TextStyle(fontWeight: FontWeight.bold),
                                                                              ),
                                                                            ),
                                                                            const SizedBox(height: 20),
                                                                            const Text(
                                                                              'Como faço para denunciar uma publicação ou um perfil na Needhere',
                                                                              style: TextStyle(fontWeight: FontWeight.bold),
                                                                            ),
                                                                            const SizedBox(height: 20),
                                                                            const Text(
                                                                              '''Se você tem uma conta da Needhere, pode denunciar um perfil ou um conteúdo na Needhere que não esteja seguindo as nossas Diretrizes da Comunidade. \n\nAtualmente, você pode denunciar uma publicação por qualquer um dos seguintes motivos: \n\n• Spam \n• Nudez ou atividade sexual \n• Discurso de ódio \n• Violência ou organizações perigosas \n• Bullying ou assédio \n• Venda de mercadorias regulamentas ou ilegais \n• Violações de propriedade intelectual \n• Automutilação ou suicídio \n• Apoio ou realização da cultura do cancelamento''',
                                                                              textAlign: TextAlign.start,
                                                                              style: TextStyle(color: Color(0xFFBCBCBA)),
                                                                            ),
                                                                            const SizedBox(height: 10),
                                                                            const Text(
                                                                              'Sua denuncia será anônima, exceto se estiver denunciando uma infração de propriedade intelectual. A conta denunciada não verá que você a denunciou.',
                                                                              textAlign: TextAlign.start,
                                                                              style: TextStyle(color: Color(0xFFBCBCBA)),
                                                                            ),
                                                                            const SizedBox(height: 10),
                                                                            const Text(
                                                                              'Denunciar uma publicação pelo Flow',
                                                                              textAlign: TextAlign.start,
                                                                            ),
                                                                            const SizedBox(height: 10),
                                                                            const Text(
                                                                              '1 Toque em acima da publicação. \n2 Toque em Denunciar. \n3 Siga as instruções da tela.',
                                                                              textAlign: TextAlign.start,
                                                                              style: TextStyle(color: Color(0xFFBCBCBA)),
                                                                            ),
                                                                            const SizedBox(height: 10),
                                                                            const Text(
                                                                              'Denunciar uma pessoa pelo perfil dela',
                                                                              textAlign: TextAlign.start,
                                                                            ),
                                                                            const SizedBox(height: 10),
                                                                            const Text('1 Toque no nome do Needzer da pessoa no flow, publicação ou bate-papo com ela. Você também pode tocar em e pesquisar o nome do Needzer para acessar o perfil dela. \n2 Toque em no canto superior esquerdo do perfil. \n3 Toque em Denunciar. \n4 Siga as instruções da tela.',
                                                                                textAlign: TextAlign.start,
                                                                                style: TextStyle(color: Color(0xFFBCBCBA))),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                            );
                                                          },
                                                        );
                                                      },
                                                      contentPadding:
                                                          EdgeInsets.zero,
                                                      dense: true,
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                },
                                child: const Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Text(
                                    'Denunciar...',
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                            const SizedBox(height: 15),
                            if (!widget.profileType.contains('mine'))
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                    isScrollControlled: true,
                                    context: context,
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10),
                                      ),
                                    ),
                                    backgroundColor: NHColors.primaryColor,
                                    builder: (context) {
                                      return SafeArea(
                                        child: SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              1.6,
                                          child: Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: Column(
                                              children: [
                                                Row(
                                                  children: [
                                                    IconButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      icon: const Icon(
                                                          Icons.arrow_back),
                                                    ),
                                                    const Spacer(),
                                                    const SizedBox(
                                                      width: 45,
                                                      child: Divider(
                                                        color: Colors.white,
                                                        thickness: 3,
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                  ],
                                                ),
                                                const SizedBox(height: 10),
                                                const Text(
                                                  'Bloquear a afadinha',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                const SizedBox(height: 30),
                                                const Text(
                                                  "O Needzer não poderá enviar mensagem para você nem encontrar o seu perfil, seus Need's na Needhere. Ela não será notificada que você a bloqueou",
                                                  style: TextStyle(
                                                      color: Color(0xFFBCBCBA)),
                                                ),
                                                const SizedBox(height: 30),
                                                Row(
                                                  children: [
                                                    const Text(
                                                        'Bloquear a afadinha'),
                                                    Checkbox(
                                                      checkColor: Colors.white,
                                                      fillColor: MaterialStateColor
                                                          .resolveWith((states) =>
                                                              NHColors
                                                                  .whiteColor),
                                                      value: true,
                                                      shape: const CircleBorder(
                                                        side: BorderSide(
                                                          style:
                                                              BorderStyle.solid,
                                                        ),
                                                      ),
                                                      onChanged: (bool? value) {
                                                        setState(() {
                                                          isChecked = value!;
                                                        });
                                                      },
                                                    ),
                                                  ],
                                                ),
                                                const SizedBox(height: 40),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 80.0,
                                                    right: 80.0,
                                                  ),
                                                  child: NHButton(
                                                    title: 'Bloquear',
                                                    disabled: false,
                                                    onTap: () {},
                                                    isExpanded: true,
                                                    backgroundColor: NHColors
                                                        .primaryGreyColor,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                },
                                child: const Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Text(
                                    'Bloquear',
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                            const SizedBox(height: 15),
                            if (widget.profileType.contains('mine'))
                              InkWell(
                                onTap: () {},
                                child: const Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Text(
                                    'Editar Informações do Perfil...',
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                            const SizedBox(height: 15),
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                                Fluttertoast.showToast(
                                  msg: 'Link copiado',
                                  textColor:
                                      NHColors.whiteColor.withOpacity(0.7),
                                  gravity: ToastGravity.CENTER,
                                  backgroundColor: NHColors.primaryColor,
                                );
                              },
                              child: const Padding(
                                padding: EdgeInsets.all(4.0),
                                child: Text(
                                  'Copiar URL do perfil',
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          },
          child: Container(
            height: 54,
            color: NHColors.primaryBlackDetailColor,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: InkWell(
                    onTap: () {
                      // int index = 0;
                      // if (index != 3) {
                      //   setState(() {
                      //     _selectedPage = 1;
                      //   });

                      //   _pageController!.animateToPage(
                      //     index,
                      //     curve: Curves.easeInOut,
                      //     duration: const Duration(milliseconds: 100),
                      //   );
                      // }

                      Navigator.pop(context);
                    },
                    customBorder: const CircleBorder(),
                    child: Card(
                      color: NHColors.primaryBlackDetailColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100)),
                      child: Image.asset(
                        'assets/icons/globe.png',
                        height: 30,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    int index = 1;
                    if (index != 3) {
                      setState(() {
                        _selectedPage = index;
                      });

                      _pageController!.animateToPage(
                        index,
                        curve: Curves.easeInOut,
                        duration: const Duration(milliseconds: 100),
                      );
                    }
                  },
                  customBorder: const CircleBorder(),
                  child: Card(
                    color: NHColors.primaryBlackDetailColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    child: Image.asset(
                      'assets/icons/more.png',
                      height: 55,
                    ),
                  ),
                ),
                const SizedBox(width: 50),
                Align(
                  alignment: Alignment.center,
                  child: InkWell(
                    onTap: () {
                      int index = 2;
                      if (index != 3) {
                        setState(() {
                          _selectedPage = index;
                        });

                        _pageController!.animateToPage(
                          index,
                          curve: Curves.easeInOut,
                          duration: const Duration(milliseconds: 100),
                        );
                      }
                    },
                    customBorder: const CircleBorder(),
                    child: Card(
                      color: NHColors.primaryBlackDetailColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100)),
                      child: Image.asset(
                        'assets/icons/discover.png',
                        height: 28,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
