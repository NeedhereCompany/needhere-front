import 'package:flutter/material.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/helpers/widgets/image_logo_generic.dart';

class NewPasswordScreen extends StatefulWidget {
  static String route = '/login';
  const NewPasswordScreen({Key? key}) : super(key: key);

  @override
  State<NewPasswordScreen> createState() => _NewPasswordScreenState();
}

class _NewPasswordScreenState extends State<NewPasswordScreen> {
  TextEditingController textEditingController = TextEditingController();
  bool hasError = false;
  String currentText = '';

  final _fKey = GlobalKey<FormState>();

  bool isChecked = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryBlackDetailColor,
        body: ListView(children: <Widget>[
          const ImageLogoGeneric(isBack: true),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Container(
              decoration: const BoxDecoration(
                color: NHColors.primaryBlackDetailColor,
              ),
              child: FormBuilder(
                key: _fKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!.needhereWithYou,
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .enterPasswordsAndConfirm,
                          style: TextStyle(
                            color: NHColors.whiteColor.withOpacity(0.7),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    NHInputField(
                      name: 'password',
                      placeHolder: AppLocalizations.of(context)!.password,
                      controller: TextEditingController(),
                      trailing: Image.asset(
                        'assets/icons/password.png',
                      ),
                      isPassword: true,
                      isRequired: true,
                      isSquareBorder: true,
                    ),
                    const SizedBox(height: 15),
                    NHInputField(
                      name: 'confirmPassword',
                      placeHolder:
                          AppLocalizations.of(context)!.confirmPassword,
                      controller: TextEditingController(),
                      trailing: Image.asset(
                        'assets/icons/password.png',
                      ),
                      isPassword: true,
                      isRequired: true,
                      isSquareBorder: true,
                    ),
                    const SizedBox(height: 50),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 80.0,
                        right: 80.0,
                      ),
                      child: NHButton(
                        title: AppLocalizations.of(context)!.continueStep,
                        disabled: true,
                        onTap: () {},
                        isExpanded: true,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
