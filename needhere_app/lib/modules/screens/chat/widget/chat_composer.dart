import 'dart:io';

import 'package:flutter/material.dart';

import 'emoji_cod/category_icons.dart';
import 'emoji_cod/config.dart';
import 'emoji_cod/emoji.dart';
import 'emoji_cod/emoji_picker.dart';

class ComposerChat extends StatefulWidget {
  const ComposerChat({Key? key}) : super(key: key);

  @override
  State<ComposerChat> createState() => _ComposerChatState();
}

class _ComposerChatState extends State<ComposerChat> {
  TextEditingController txtMessage = TextEditingController();

  bool emojiShowing = false;

  _onEmojiSelected(Emoji emoji) {
    txtMessage
      ..text += emoji.emoji
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: txtMessage.text.length));
  }

  _onBackspacePressed() {
    txtMessage
      ..text = txtMessage.text.characters.skipLast(1).toString()
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: txtMessage.text.length));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          height: 100,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 14),
                  height: 60,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Row(
                    children: [
                      GestureDetector(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: Image.asset("assets/icons/emoji_icon.png"),
                        ),
                        onTap: () {
                          setState(() {
                            emojiShowing = !emojiShowing;
                          });
                        },
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.multiline,
                          textInputAction: TextInputAction.newline,
                          // expands: true,
                          // minLines: null,
                          maxLines: null,
                          controller: txtMessage,
                          decoration: const InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Conversar',
                            hintStyle: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 12,
                              color: Color(0xfff5f5f5),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: Image.asset("assets/icons/photo_icon.png"),
                        ),
                        onTap: () {
                          setState(() {
                            emojiShowing = !emojiShowing;
                          });
                        },
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      // Icon(
                      //   Icons.attach_file,
                      //   color: Colors.grey[500],
                      // )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 16,
              ),
              GestureDetector(
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: Image.asset("assets/icons/send_icon.png"),
                  ),
                  onTap: () async {})
            ],
          ),
        ),
        Offstage(
          offstage: !emojiShowing,
          child: SizedBox(
            height: 250,
            child: EmojiPicker(
                onEmojiSelected: (Category category, Emoji emoji) {
                  _onEmojiSelected(emoji);
                },
                onBackspacePressed: _onBackspacePressed,
                config: Config(
                    columns: 7,
                    // Issue: https://github.com/flutter/flutter/issues/28894
                    emojiSizeMax: 32 * (Platform.isIOS ? 1.30 : 1.0),
                    verticalSpacing: 0,
                    horizontalSpacing: 0,
                    initCategory: Category.RECENT,
                    bgColor: const Color(0xFFF2F2F2),
                    indicatorColor: Colors.blue,
                    iconColor: Colors.grey,
                    iconColorSelected: Colors.blue,
                    progressIndicatorColor: Colors.blue,
                    backspaceColor: Colors.blue,
                    showRecentsTab: true,
                    recentsLimit: 28,
                    noRecentsText: 'No Recents',
                    noRecentsStyle:
                        const TextStyle(fontSize: 20, color: Colors.black26),
                    tabIndicatorAnimDuration: kTabScrollDuration,
                    categoryIcons: const CategoryIcons(),
                    buttonMode: ButtonMode.MATERIAL)),
          ),
        ),
      ],
    );
  }
}
