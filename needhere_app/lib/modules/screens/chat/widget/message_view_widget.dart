import 'package:flutter/material.dart';

import 'message_widget.dart';

class MessageViewWidget extends StatelessWidget {
  const MessageViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        physics: const ClampingScrollPhysics(),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Barbara Souza',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 5),
              const Text(
                "barbaraSouzaa - 1,4 Needzer's",
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 5),
              const Text(
                'Vocês não se seguem mutuamente na Needhere',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 5),
              const Text(
                'A Needhere protege suas conversas com a criptografia de ponta a ponta, além de não vendermos seus dados.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: 110,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(9.0),
                  border:
                      Border.all(width: 1.0, color: const Color(0x5cffffff)),
                ),
                child: const Center(
                  child: Text(
                    'Ver Needzer',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 10,
                      color: Color(0xfff3f3f3),
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          ListView.builder(
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemCount: 10,
              itemBuilder: (_, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: MessageWidget(
                    isMe: index % 2 == 0,
                    text:
                        'Magna et nulla ut incididunt exercitation. Fugiat laboris aute veniam deserunt excepteur dolore ea. Ipsum ex ex et deserunt duis cillum tempor ut ipsum ea consectetur.',
                  ),
                );
              })
        ],
      ),
    );
  }
}
