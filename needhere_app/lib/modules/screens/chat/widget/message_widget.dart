import 'package:flutter/material.dart';

import '../../../../global_const.dart';

class MessageWidget extends StatelessWidget {
  const MessageWidget({Key? key, this.isMe = false, this.text = ""})
      : super(key: key);

  final bool isMe;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
            padding: const EdgeInsets.all(10),
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.8),
            decoration: BoxDecoration(
                color: isMe ? myMessageCardColor : otherMessageCardColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(16),
                  topRight: const Radius.circular(16),
                  bottomLeft: Radius.circular(isMe ? 12 : 0),
                  bottomRight: Radius.circular(isMe ? 0 : 12),
                )),
            child: Column(
              children: [
                Text(text ?? "", style: const TextStyle(color: Colors.white)),
                Align(
                  alignment: Alignment.topRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Text("13:22",
                          style: TextStyle(color: Colors.white, fontSize: 10)),
                      Icon(
                        Icons.done_all,
                        size: 20,
                        color: Colors.green,
                      )
                    ],
                  ),
                )
              ],
            )),
      ],
    );
  }
}
