import 'package:flutter/material.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/chat/widget/chat_composer.dart';
import 'package:needhere_app/modules/screens/chat/widget/message_view_widget.dart';

class ChatConversationView extends StatefulWidget {
  const ChatConversationView({Key? key}) : super(key: key);

  @override
  State<ChatConversationView> createState() => _ChatConversationViewState();
}

class _ChatConversationViewState extends State<ChatConversationView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: NHColors.primaryColor,
          leading: InkWell(
            customBorder: const CircleBorder(),
            onTap: () {
              Navigator.pop(context);
            },
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100)),
              color: Colors.transparent,
              child: const Icon(
                Icons.arrow_back_ios,
                size: 18,
              ),
            ),
          ),
        ),
        body: Column(
          children: const [
            Expanded(child: MessageViewWidget()),
            ComposerChat()
          ],
        ),
      ),
    );
  }
}
