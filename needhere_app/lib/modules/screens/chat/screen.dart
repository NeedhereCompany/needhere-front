import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/chat/view/conversation/view.dart';

class ChatScreen extends StatefulWidget {
  static String route = '/chat';
  const ChatScreen({Key? key}) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    _init();
  }

  _init() {
    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: isLoading
          ? Scaffold(
              backgroundColor: Colors.black,
              body: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/icons8-cavalo-marinho-96.png',
                      height: 500,
                    ),
                    const Text('Privacidade, conectividade e criptografia')
                  ],
                ),
              ),
            )
          : Scaffold(
              backgroundColor: NHColors.primaryColor,
              appBar: AppBar(
                backgroundColor: NHColors.primaryColor,
                elevation: 0,
                leading: InkWell(
                  customBorder: const CircleBorder(),
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100)),
                    child: const Icon(
                      Icons.arrow_back_ios,
                      size: 20,
                    ),
                  ),
                ),
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text(
                      'Needtalk',
                      style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.bold,
                        color: NHColors.secondaryColor,
                      ),
                    ),
                    SizedBox(width: 4),
                    Text(
                      'Talks',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: NHColors.secondaryColor,
                      ),
                    )
                  ],
                ),
              ),
              body: ListView.builder(
                itemCount: 20,
                itemBuilder: (context, index) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 20),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return const ChatConversationView();
                          }));
                        },
                        child: SizedBox(
                          height: 60,
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  // CircleAvatar(
                                  //   backgroundColor: Colors.white,
                                  //   radius: 30,
                                  // ),
                                  ClipPolygon(
                                    sides: 8,
                                    rotate: 112.0,
                                    boxShadows: [
                                      PolygonBoxShadow(
                                          color: Colors.black, elevation: 1.0),
                                      PolygonBoxShadow(
                                          color: Colors.grey, elevation: 5.0)
                                    ],
                                    child: Image.asset(
                                      'assets/images/slides/slide-image-2.png',
                                      fit: BoxFit.fitWidth,
                                    ),
                                  ),
                                  const Positioned(
                                    bottom: 5,
                                    right: -1,
                                    child: CircleAvatar(
                                      radius: 7,
                                      child: CircleAvatar(
                                        radius: 5,
                                        backgroundColor:
                                            NHColors.secondaryColor,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text(
                                    'NeedHer user ',
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                  SizedBox(
                                    width: 180,
                                    child: Text(
                                      'Dolore amet occaecat pariatur culpa ipsum commodo pariatur exercitation commodo aute.',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              const Spacer(),
                              Column(
                                children: const [
                                  Text('16:45'),
                                  // Spacer(),
                                  SizedBox(height: 20),
                                  CircleAvatar(
                                    backgroundColor: NHColors.secondaryColor,
                                    radius: 9,
                                    child: Text(
                                      '3',
                                      style: TextStyle(
                                        fontSize: 13,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ));
                },
              ),
              // body: ListView.builder(
              //   itemBuilder: (context, index) {
              //     return Padding(
              //       padding: const EdgeInsets.all(8.0),
              //       child: ListTile(
              //         leading: Stack(
              //           children: [
              //             CircleAvatar(
              //               backgroundColor: Colors.white,
              //               radius: 30,
              //             ),
              //             Positioned(
              //               bottom: 5,
              //               right: -1,
              //               child: CircleAvatar(
              //                 radius: 7,
              //                 child: CircleAvatar(
              //                   radius: 5,
              //                   backgroundColor: NHColors.secondaryColor,
              //                 ),
              //               ),
              //             )
              //           ],
              //         ),
              //         title: Text(
              //           'Fullname',
              //           style: TextStyle(
              //             fontWeight: FontWeight.bold,
              //             fontSize: 18,
              //           ),
              //         ),
              //         subtitle: Column(
              //           children: [
              //             SizedBox(height: 10),
              //             Text(
              //               'Dolore amet occaecat pariatur culpa ipsum commodo pariatur exercitation commodo aute.',
              //               maxLines: 1,
              //               overflow: TextOverflow.ellipsis,
              //               style: TextStyle(
              //                 fontSize: 14,
              //               ),
              //             ),
              //           ],
              //         ),
              //         trailing: Column(
              //           children: [
              //             Text('16:45'),
              //             // Spacer(),
              //             SizedBox(height: 20),
              //             CircleAvatar(
              //               backgroundColor: NHColors.secondaryColor,
              //               radius: 9,
              //               child: Text(
              //                 '3',
              //                 style: TextStyle(
              //                   fontSize: 13,
              //                   color: Colors.black,
              //                   fontWeight: FontWeight.bold,
              //                 ),
              //               ),
              //             ),
              //           ],
              //         ),
              //         onTap: () {
              //           Navigator.push(context, MaterialPageRoute(builder: (context) {
              //             return const ChatConversationView();
              //           }));
              //         },
              //       ),
              //     );
              //   },
              // ),
            ),
    );
  }
}
