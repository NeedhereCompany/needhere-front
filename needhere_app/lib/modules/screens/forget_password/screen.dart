import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/helpers/widgets/image_logo_generic.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/confirmation_code/screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ForgetPasswordScreen extends StatefulWidget {
  static String route = '/login';
  const ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  bool isChecked = false;

  final _fKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryBlackDetailColor,
        body: ListView(children: <Widget>[
          const ImageLogoGeneric(isBack: true),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Container(
              decoration: const BoxDecoration(
                color: NHColors.primaryBlackDetailColor,
              ),
              child: FormBuilder(
                key: _fKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          '${AppLocalizations.of(context)!.forgetPassword}?',
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .instructionsForgetPassLabel,
                          style: TextStyle(
                            color: NHColors.whiteColor.withOpacity(0.7),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    NHInputField(
                      name: 'email',
                      placeHolder: 'Email',
                      controller: TextEditingController(),
                      trailing: Image.asset(
                        'assets/icons/email.png',
                      ),
                      textInputType: TextInputType.emailAddress,
                      isRequired: true,
                      isEmail: true,
                      isSquareBorder: true,
                    ),
                    const SizedBox(height: 50),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 80.0,
                        right: 80.0,
                      ),
                      child: NHButton(
                        title: AppLocalizations.of(context)!.continueStep,
                        disabled: true,
                        onTap: () {
                          if (_fKey.currentState?.saveAndValidate() ?? false) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return const ConfirmationCodeScreen();
                                },
                              ),
                            );
                          }
                        },
                        isExpanded: true,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
