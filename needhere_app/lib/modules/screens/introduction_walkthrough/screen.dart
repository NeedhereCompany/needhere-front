import 'package:flutter/material.dart';
import 'package:needhere_app/modules/screens/home/screen.dart';
import 'package:needhere_app/modules/screens/introduction_walkthrough/views/fourth_introduction.dart';
import 'package:needhere_app/modules/screens/introduction_walkthrough/views/third_introduction.dart';

import 'views/fifth_introduction.dart';
import 'views/first_introduction.dart';
import 'views/second_introduction.dart';

class IntroductionWalkthroughScreen extends StatefulWidget {
  const IntroductionWalkthroughScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<IntroductionWalkthroughScreen> createState() =>
      _IntroductionWalkthroughScreenState();
}

class _IntroductionWalkthroughScreenState
    extends State<IntroductionWalkthroughScreen> {
  PageController? pageController;

  int _selectedPageIndex = 0;

  @override
  void initState() {
    pageController = PageController(
      initialPage: 0,
      keepPage: true,
    );

    super.initState();
  }

  void nextStep(int index) async {
    if (index < 4) {
      setState(
        () {
          _selectedPageIndex = index + 1;
        },
      );

      await pageController!.animateToPage(
        _selectedPageIndex,
        duration: const Duration(milliseconds: 800),
        curve: Curves.decelerate,
      );
    } else {
      debugPrint('end');
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) {
          return const HomeScreen();
        }),
      );
    }
  }

  void previousStep(int index) async {
    if (index > 0) {
      setState(
        () {
          _selectedPageIndex = index - 1;
        },
      );

      await pageController!.animateToPage(
        _selectedPageIndex,
        duration: const Duration(milliseconds: 800),
        curve: Curves.decelerate,
      );
    } else {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: pageController,
        children: [
          FirstIntroductionScreen(
            title: 'Introdução',
            previousStep: () => previousStep(_selectedPageIndex),
            nextStep: () => nextStep(_selectedPageIndex),
          ),
          SecondIntroductionScreen(
            title: 'Introdução',
            previousStep: () => previousStep(_selectedPageIndex),
            nextStep: () => nextStep(_selectedPageIndex),
          ),
          ThirdIntroductionScreen(
            title: 'Permissões',
            previousStep: () => previousStep(_selectedPageIndex),
            nextStep: () => nextStep(_selectedPageIndex),
          ),
          FourthIntroductionScreen(
            title: 'Focus',
            previousStep: () => previousStep(_selectedPageIndex),
            nextStep: () => nextStep(_selectedPageIndex),
          ),
          FifthIntroductionScreen(
            title: 'Sugestões',
            previousStep: () => previousStep(_selectedPageIndex),
            nextStep: () => nextStep(_selectedPageIndex),
          ),
        ],
      ),
    );
  }
}
