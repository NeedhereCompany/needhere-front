import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class FifthIntroductionScreen extends StatefulWidget {
  final String title;
  final Function previousStep;
  final Function nextStep;

  const FifthIntroductionScreen({
    Key? key,
    required this.title,
    required this.previousStep,
    required this.nextStep,
  }) : super(key: key);

  @override
  State<FifthIntroductionScreen> createState() =>
      _FifthIntroductionScreenState();
}

class _FifthIntroductionScreenState extends State<FifthIntroductionScreen> {
  TextEditingController textEditingController = TextEditingController();
  final faker = Faker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_outlined),
          onPressed: () {
            widget.previousStep();
          },
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            fontSize: 15,
          ),
        ),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              widget.nextStep();
            },
            style: ButtonStyle(
              foregroundColor: MaterialStateColor.resolveWith(
                  (states) => NHColors.secondaryColor),
            ),
            child: const Text('Concluído'),
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 10),
          const Padding(
            padding: EdgeInsets.all(25.0),
            child: Text(
              'Ao seguir um Needzer, você verá os Needs dessa pessoa em seu universo',
              style: TextStyle(
                color: NHColors.primaryGreyColor,
                fontSize: 17,
              ),
            ),
          ),
          const SizedBox(height: 30),
          const Text(
            'O universo Needhere te espera',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: SizedBox(
              width: double.infinity,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                // physics: ,
                child: Row(
                  children: [
                    for (var i = 0; i < 10; i++)
                      SizedBox(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: SizedBox(
                            height: 200,
                            width: 150,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 70,
                                  child: ClipPolygon(
                                    sides: 8,
                                    rotate: 112.0,
                                    boxShadows: [
                                      PolygonBoxShadow(
                                          color: Colors.black, elevation: 1.0),
                                      PolygonBoxShadow(
                                          color: Colors.grey, elevation: 5.0)
                                    ],
                                    child: Image.asset(
                                      'assets/images/slides/slide-image-2.png',
                                      fit: BoxFit.fitWidth,
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 25),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 20.0,
                                    right: 20.0,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              '${faker.person.firstName()} ${faker.person.lastName()}',
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12),
                                            ),
                                          ),
                                          if (i % 2 == 0)
                                            Opacity(
                                              opacity: 0.7,
                                              child: Image.asset(
                                                'assets/icons/verified.png',
                                                height: 18,
                                              ),
                                            )
                                        ],
                                      ),
                                      const SizedBox(height: 3),
                                      Text(
                                        '@${faker.person.firstName().toLowerCase()}',
                                        textAlign: TextAlign.left,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12),
                                      ),
                                      const SizedBox(height: 5),
                                      const Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          'Não segue você',
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            color: NHColors.primaryGreyColor,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 10),
                                SizedBox(
                                  height: 30,
                                  width: 80,
                                  child: NHButton(
                                    title: 'Seguir',
                                    disabled: true,
                                    onTap: () {},
                                    fontSize: 13,
                                    isRounded: false,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
