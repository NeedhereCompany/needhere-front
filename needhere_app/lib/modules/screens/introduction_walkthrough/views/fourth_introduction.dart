import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class FourthIntroductionScreen extends StatefulWidget {
  final String title;
  final Function previousStep;
  final Function nextStep;

  const FourthIntroductionScreen({
    Key? key,
    required this.title,
    required this.previousStep,
    required this.nextStep,
  }) : super(key: key);

  @override
  State<FourthIntroductionScreen> createState() =>
      _FourthIntroductionScreenState();
}

class _FourthIntroductionScreenState extends State<FourthIntroductionScreen> {
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_outlined),
          onPressed: () {
            widget.previousStep();
          },
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            fontSize: 15,
          ),
        ),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              widget.nextStep();
            },
            style: ButtonStyle(
              foregroundColor: MaterialStateColor.resolveWith(
                  (states) => NHColors.secondaryColor),
            ),
            child: const Text('Continuar'),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 45.0),
                child: Text(
                  'Personalize o mundo do seu jeito, tenha um mundo único, exclusivo e focado',
                  style: TextStyle(
                    color: NHColors.primaryGreyColor,
                    fontSize: 15,
                  ),
                ),
              ),
              const SizedBox(height: 35),
              const Text(
                'O que não pode faltar?',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 5),
              FormBuilderChoiceChip(
                name: 'choice_chip_1',
                spacing: 6,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                  ),
                  border: UnderlineInputBorder(borderSide: BorderSide.none),
                ),
                options: const [
                  FormBuilderFieldOption(
                    value: '1',
                    child: Text(
                      'Música',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '2',
                    child: Text(
                      'Pop',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '3',
                    child: Text(
                      'K-Pop',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '4',
                    child: Text(
                      'Esportes',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '5',
                    child: Text(
                      'Gaming',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '6',
                    child: Text(
                      'Atores',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '7',
                    child: Text(
                      'Filmes e TV',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '8',
                    child: Text(
                      'Rap',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '9',
                    child: Text(
                      'Celebridades',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '10',
                    child: Text(
                      'Música',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '11',
                    child: Text(
                      'Pagode',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              const Text(
                'Esportes',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 5),
              FormBuilderChoiceChip(
                name: 'choice_chip_2',
                spacing: 6,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                  ),
                  border: UnderlineInputBorder(borderSide: BorderSide.none),
                ),
                options: const [
                  FormBuilderFieldOption(
                    value: '12',
                    child: Text(
                      'Flamengo',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '13',
                    child: Text(
                      'Futebol',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '14',
                    child: Text(
                      'Neymar',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '15',
                    child: Text(
                      'Brasil',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '16',
                    child: Text(
                      'NBA',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '17',
                    child: Text(
                      'Vasco da Gama',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '18',
                    child: Text(
                      'UFC',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '19',
                    child: Text(
                      'Formula 1',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '20',
                    child: Text(
                      'Grêmio',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '21',
                    child: Text(
                      'Fluminense',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '22',
                    child: Text(
                      'Muay Thai',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '23',
                    child: Text(
                      'Santos',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              const Text(
                'Gaming',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 5),
              FormBuilderChoiceChip(
                name: 'choice_chip_3',
                spacing: 6,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                  ),
                  border: UnderlineInputBorder(borderSide: BorderSide.none),
                ),
                options: const [
                  FormBuilderFieldOption(
                    value: '24',
                    child: Text(
                      'Vídeo Games',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '25',
                    child: Text(
                      'Xbox',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '26',
                    child: Text(
                      'Call of Dutty',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                  FormBuilderFieldOption(
                    value: '27',
                    child: Text(
                      'Fifa',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
