import 'package:flutter/material.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class FirstIntroductionScreen extends StatefulWidget {
  final String title;
  final Function previousStep;
  final Function nextStep;

  const FirstIntroductionScreen(
      {Key? key,
      required this.title,
      required this.previousStep,
      required this.nextStep})
      : super(key: key);

  @override
  State<FirstIntroductionScreen> createState() =>
      _FirstIntroductionScreenState();
}

class _FirstIntroductionScreenState extends State<FirstIntroductionScreen> {
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_outlined),
          onPressed: () {
            widget.previousStep();
          },
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            fontSize: 15,
          ),
        ),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              widget.nextStep();
            },
            style: ButtonStyle(
              foregroundColor: MaterialStateColor.resolveWith(
                  (states) => NHColors.secondaryColor),
            ),
            child: const Text('Continuar'),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Qual é o seu verdadeiro eu?',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(45.0),
              child: Text(
                'Sinta cada momento, agora livre e totalmente sem censuras',
                style: TextStyle(
                  color: NHColors.primaryGreyColor,
                  fontSize: 15,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 45,
                right: 45,
              ),
              child: NHInputField(
                name: 'userName',
                controller: textEditingController,
                preffixIcon: const Text(
                  '@',
                  style: TextStyle(
                    fontSize: 15,
                    color: NHColors.primaryGreyColor,
                  ),
                ),
                isRequired: true,
                borderType: UnderlineInputBorder,
                maxLength: 30,
              ),
            )
          ],
        ),
      ),
    );
  }
}
