import 'package:flutter/material.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class ThirdIntroductionScreen extends StatefulWidget {
  final String title;
  final Function previousStep;
  final Function nextStep;

  const ThirdIntroductionScreen({
    Key? key,
    required this.title,
    required this.previousStep,
    required this.nextStep,
  }) : super(key: key);

  @override
  State<ThirdIntroductionScreen> createState() =>
      _ThirdIntroductionScreenState();
}

class _ThirdIntroductionScreenState extends State<ThirdIntroductionScreen> {
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_outlined),
          onPressed: () {
            widget.previousStep();
          },
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            fontSize: 15,
          ),
        ),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              widget.nextStep();
            },
            style: ButtonStyle(
              foregroundColor: MaterialStateColor.resolveWith(
                  (states) => NHColors.secondaryColor),
            ),
            child: const Text('Continuar'),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Encontre seus amigos',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(45.0),
              child: Text(
                'Permita acesso a seus contatos e\njunte-se a seus amigos',
                style: TextStyle(
                  color: NHColors.primaryGreyColor,
                  fontSize: 15,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 80.0,
                right: 80.0,
              ),
              child: NHButton(
                title: 'Permitir acesso',
                disabled: true,
                onTap: () {},
                fontSize: 13,
                isExpanded: true,
              ),
            ),
            const SizedBox(height: 6),
            Padding(
              padding: const EdgeInsets.only(
                left: 100.0,
                right: 100.0,
              ),
              child: NHButton(
                title: 'Não permitir',
                disabled: true,
                onTap: () {},
                fontSize: 13,
                isExpanded: true,
                backgroundColor: NHColors.primaryGreyColor,
              ),
            ),
            const SizedBox(height: 10),
            const Divider(),
            const SizedBox(height: 10),
            const Padding(
              padding: EdgeInsets.only(
                left: 45.0,
                right: 45.0,
              ),
              child: Text(
                'A Needhere é a primeira aplicação que respeita e tem sua privacidade como uma de nossas prioridades',
                style: TextStyle(
                  color: NHColors.primaryGreyColor,
                  fontSize: 15,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
