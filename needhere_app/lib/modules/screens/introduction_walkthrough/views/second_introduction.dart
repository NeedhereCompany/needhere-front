import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class SecondIntroductionScreen extends StatefulWidget {
  final String title;
  final Function previousStep;
  final Function nextStep;

  const SecondIntroductionScreen({
    Key? key,
    required this.title,
    required this.previousStep,
    required this.nextStep,
  }) : super(key: key);

  @override
  State<SecondIntroductionScreen> createState() =>
      _SecondIntroductionScreenState();
}

class _SecondIntroductionScreenState extends State<SecondIntroductionScreen> {
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_outlined),
          onPressed: () {
            widget.previousStep();
          },
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            fontSize: 15,
          ),
        ),
        centerTitle: true,
        actions: [
          TextButton(
            onPressed: () {
              widget.nextStep();
            },
            style: ButtonStyle(
              foregroundColor: MaterialStateColor.resolveWith(
                  (states) => NHColors.secondaryColor),
            ),
            child: const Text('Continuar'),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(25.0),
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 100,
                    child: ClipPolygon(
                      sides: 8,
                      rotate: 112.0,
                      boxShadows: [
                        PolygonBoxShadow(color: Colors.black, elevation: 1.0),
                        PolygonBoxShadow(color: Colors.grey, elevation: 5.0)
                      ],
                      child: Container(
                        color: Colors.white,
                        child: const Icon(
                          Icons.person,
                          size: 50,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Nome completo',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(45.0),
                    child: Text(
                      'O que lhe torna humano? Fuja das rotinas, se divirta com esse processo',
                      style: TextStyle(
                        color: NHColors.primaryGreyColor,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  NHInputField(
                    name: 'biography',
                    controller: textEditingController,
                    placeHolder: 'Sua biografia (opcional)',
                    maxLength: 150,
                    isRequired: true,
                    borderType: UnderlineInputBorder,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
