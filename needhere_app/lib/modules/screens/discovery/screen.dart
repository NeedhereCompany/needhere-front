import 'package:flutter/material.dart';
import 'package:flutter_polygon/flutter_polygon.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';

class DiscoveryScreen extends StatefulWidget {
  const DiscoveryScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<DiscoveryScreen> createState() => _DiscoveryScreenState();
}

class _DiscoveryScreenState extends State<DiscoveryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 22.0,
              top: 6.0,
              right: 6.0,
              bottom: 6.0,
            ),
            child: Row(
              children: [
                Image.asset(
                  'assets/icons/discover.png',
                  height: 25,
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      physics: const BouncingScrollPhysics(),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Row(
                          children: [
                            for (var i = 1; i < 4; i++)
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 2,
                                    top: 20,
                                    right: 2,
                                  ),
                                  child: Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/discover/$i.png"),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        width: 140,
                                      ),
                                      Positioned(
                                        top: -22,
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              height: 45,
                                              child: ClipPolygon(
                                                sides: 8,
                                                rotate: 112.0,
                                                boxShadows: [
                                                  PolygonBoxShadow(
                                                      color: Colors.black,
                                                      elevation: 1.0),
                                                  PolygonBoxShadow(
                                                      color: Colors.grey,
                                                      elevation: 5.0)
                                                ],
                                                child: Image.asset(
                                                  'assets/images/slides/slide-image-2.png',
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ),
                                            ),
                                            const SizedBox(width: 8),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Column(
                                                children: [
                                                  InkWell(
                                                    onTap: () {},
                                                    child: const Text(
                                                      'Seguir',
                                                      style: TextStyle(
                                                        color: NHColors
                                                            .secondaryColor,
                                                        fontSize: 12,
                                                      ),
                                                    ),
                                                  ),
                                                  const Text(''),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Text(
                    'Recomendados a você',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      physics: const BouncingScrollPhysics(),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Row(
                          children: [
                            for (var i = 4; i < 7; i++)
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 2,
                                    top: 20,
                                    right: 2,
                                  ),
                                  child: Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/discover/$i.png"),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        width: 140,
                                      ),
                                      Positioned(
                                        top: -22,
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              height: 45,
                                              child: ClipPolygon(
                                                sides: 8,
                                                rotate: 112.0,
                                                boxShadows: [
                                                  PolygonBoxShadow(
                                                      color: Colors.black,
                                                      elevation: 1.0),
                                                  PolygonBoxShadow(
                                                      color: Colors.grey,
                                                      elevation: 5.0)
                                                ],
                                                child: Image.asset(
                                                  'assets/images/slides/slide-image-2.png',
                                                  fit: BoxFit.fitWidth,
                                                ),
                                              ),
                                            ),
                                            const SizedBox(width: 8),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Column(
                                                children: [
                                                  InkWell(
                                                    onTap: () {},
                                                    child: const Text(
                                                      'Seguir',
                                                      style: TextStyle(
                                                        color: NHColors
                                                            .secondaryColor,
                                                        fontSize: 12,
                                                      ),
                                                    ),
                                                  ),
                                                  const Text(''),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
