import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:needhere_app/app/helpers/widgets/image_logo_generic.dart';
import 'package:needhere_app/app/helpers/widgets/input_field.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:needhere_app/modules/screens/introduction_walkthrough/screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ConfirmationEmailScreen extends StatefulWidget {
  static String route = '/login';
  const ConfirmationEmailScreen({Key? key}) : super(key: key);

  @override
  _ConfirmationEmailScreenState createState() =>
      _ConfirmationEmailScreenState();
}

class _ConfirmationEmailScreenState extends State<ConfirmationEmailScreen> {
  TextEditingController textEditingController = TextEditingController();
  bool hasError = false;
  String currentText = '';
  final formKey = GlobalKey<FormState>();

  final _fKey = GlobalKey<FormBuilderState>();

  bool isChecked = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryBlackDetailColor,
        body: ListView(children: <Widget>[
          const ImageLogoGeneric(isBack: true),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Container(
              decoration: const BoxDecoration(
                color: NHColors.primaryBlackDetailColor,
              ),
              child: FormBuilder(
                key: _fKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!.confirmEmailLabel,
                          style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!.geetingRealUniverse,
                          style: TextStyle(
                            color: NHColors.whiteColor.withOpacity(0.7),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    NHInputField(
                      name: 'email',
                      placeHolder: 'Email',
                      controller: TextEditingController(),
                      textInputType: TextInputType.emailAddress,
                      trailing: Image.asset('assets/icons/email.png'),
                      isRequired: true,
                      isEmail: true,
                      isSquareBorder: true,
                    ),
                    const SizedBox(height: 60),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 80.0,
                        right: 80.0,
                      ),
                      child: NHButton(
                        title: AppLocalizations.of(context)!.continueStep,
                        disabled: true,
                        onTap: () {
                          if (_fKey.currentState?.saveAndValidate() ?? false) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return const IntroductionWalkthroughScreen();
                                },
                              ),
                            );
                          }
                        },
                        isExpanded: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ]),

        // body: SizedBox(
        //   width: double.infinity,
        //   height: MediaQuery.of(context).size.height,
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       Padding(
        //         padding: const EdgeInsets.all(40.0),
        //         child: Column(
        //           children: [
        //             Image.asset(
        //               'assets/images/logo.png',
        //               height: 125,
        //             )
        //           ],
        //         ),
        //       ),
        //       Expanded(
        //         child: SingleChildScrollView(
        //           child: Container(
        //             height: MediaQuery.of(context).size.height,
        //             decoration: BoxDecoration(
        //               color: NHColors.primaryBlackDetailColor,
        //               borderRadius: const BorderRadius.only(
        //                 topLeft: Radius.circular(20),
        //                 topRight: Radius.circular(20),
        //               ),
        //             ),
        //             child: FormBuilder(
        //               key: _fKey,
        //               autovalidateMode: AutovalidateMode.always,
        //               child: Column(
        //                 children: [
        //                   Padding(
        //                     padding: const EdgeInsets.only(
        //                       left: 25.0,
        //                       top: 15.0,
        //                       right: 25.0,
        //                       bottom: 15.0,
        //                     ),
        //                     child: Row(
        //                       children: const [
        //                         Text(
        //                           'Confirme seu email',
        //                           style: TextStyle(
        //                             fontSize: 18,
        //                             fontWeight: FontWeight.w700,
        //                           ),
        //                         ),
        //                       ],
        //                     ),
        //                   ),
        //                   Padding(
        //                     padding: const EdgeInsets.only(
        //                       left: 25.0,
        //                       top: 15.0,
        //                       right: 25.0,
        //                       bottom: 25.0,
        //                     ),
        //                     child: Row(
        //                       children: [
        //                         Text(
        //                           'Seja real e viva o melhor do universo Needhere',
        //                           style: TextStyle(
        //                             color:
        //                                 NHColors.whiteColor.withOpacity(0.7),
        //                           ),
        //                         ),
        //                       ],
        //                     ),
        //                   ),
        //                   Padding(
        //                     padding: const EdgeInsets.only(
        //                       left: 25.0,
        //                       right: 25.0,
        //                       bottom: 5.0,
        //                     ),
        //                     child: SingleChildScrollView(
        //                       child: Column(
        //                         children: [
        //                           NHInputField(
        //                             name: 'email',
        //                             placeHolder: 'Email',
        //                             controller: TextEditingController(),
        //                             textInputType: TextInputType.emailAddress,
        //                             trailing:
        //                                 Image.asset('assets/icons/email.png'),
        //                             validators: [
        //                               FormBuilderValidators.required(
        //                                 context,
        //                                 errorText:
        //                                     'O campo de Email é obrigatório',
        //                               ),
        //                               FormBuilderValidators.email(
        //                                 context,
        //                                 errorText: 'Digite um Email válido',
        //                               ),
        //                             ],
        //                           ),
        //                           const SizedBox(height: 10),
        //                         ],
        //                       ),
        //                     ),
        //                   ),
        //                   const SizedBox(height: 10),
        //                   Padding(
        //                     padding: const EdgeInsets.only(
        //                       left: 120.0,
        //                       right: 120.0,
        //                     ),
        //                     child: NHButton(
        //                       title: 'Continuar',
        //                       disabled: true,
        //                       onTap: () {
        //                         if (_fKey.currentState?.saveAndValidate() ??
        //                             false) {
        //                           Navigator.push(
        //                             context,
        //                             MaterialPageRoute(
        //                               builder: (context) {
        //                                 return IntroductionWalkthroughScreen();
        //                               },
        //                             ),
        //                           );
        //                         }
        //                       },
        //                       fontSize: 12,
        //                       isExpanded: true,
        //                     ),
        //                   ),
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
      ),
    );
  }
}
