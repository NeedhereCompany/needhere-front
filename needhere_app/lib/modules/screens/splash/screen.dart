import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:needhere_app/modules/screens/home/screen.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/state.dart';
import 'package:needhere_app/modules/user/presentation/screens/login/screen.dart';
import 'package:progress_indicators/progress_indicators.dart';

import '../../user/presentation/cubit/validate/cubit.dart';

class SplashScreen extends StatefulWidget {
  static String route = '/splash';

  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    callLogin();
  }

  void callLogin() {
    Future.delayed(
      const Duration(milliseconds: 2000),
      () {
        // Navigator.pushReplacementNamed(
        //   context,
        //   LoginScreen.route,
        // );

        // ref.read(validateNotifierProvider.notifier).validate();

        context.read<ValidateCubit>().validate();
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // ref.listen<AuthenticateState>(
    //   validateNotifierProvider,
    //   (previous, state) {
    //     state.maybeWhen(
    //       authenticated: (user) {
    //         if (user != null) {
    //           Navigator.pushReplacementNamed(
    //             context,
    //             HomeScreen.route,
    //           );
    //         } else {
    //           Navigator.pushReplacementNamed(
    //             context,
    //             LoginScreen.route,
    //           );
    //         }
    //       },
    //       failed: (ex) {
    //         Navigator.pushReplacementNamed(
    //           context,
    //           LoginScreen.route,
    //         );
    //       },
    //       orElse: () {
    //         Navigator.pushReplacementNamed(
    //           context,
    //           LoginScreen.route,
    //         );
    //       },
    //     );
    //   },
    // );

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: BlocConsumer<ValidateCubit, AuthenticateState>(
            listener: (context, state) {
          if (state is AuthenticatedState) {
            if (state.user != null) {
              Navigator.pushReplacementNamed(
                context,
                HomeScreen.route,
              );
            } else {
              Navigator.pushReplacementNamed(
                context,
                LoginScreen.route,
              );
            }
          } else if (state is AuthFailState) {
            Navigator.pushReplacementNamed(
              context,
              LoginScreen.route,
            );
          } else {
            Navigator.pushReplacementNamed(
              context,
              LoginScreen.route,
            );
          }
        }, builder: (context, state) {
          return Stack(
            fit: StackFit.expand,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/logo.png',
                    height: 125,
                  ),
                  const SizedBox(height: 10),
                  GlowingProgressIndicator(
                    child: Image.asset(
                      'assets/images/beta.png',
                      height: 25,
                    ),
                  ),
                ],
              ),
            ],
          );
        }),
      ),
    );
  }
}
