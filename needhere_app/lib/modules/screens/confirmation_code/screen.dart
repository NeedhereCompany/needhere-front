import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:needhere_app/app/utils/theme/colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:needhere_app/app/helpers/widgets/button.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:needhere_app/modules/screens/new_password/screen.dart';
import 'package:needhere_app/app/helpers/widgets/image_logo_generic.dart';

class ConfirmationCodeScreen extends StatefulWidget {
  static String route = '/login';
  const ConfirmationCodeScreen({Key? key}) : super(key: key);

  @override
  _ConfirmationCodeScreenState createState() => _ConfirmationCodeScreenState();
}

class _ConfirmationCodeScreenState extends State<ConfirmationCodeScreen> {
  TextEditingController textEditingController = TextEditingController();
  bool hasError = false;
  String currentText = '';
  final _fKey = GlobalKey<FormBuilderState>();

  bool isChecked = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: NHColors.primaryBlackDetailColor,
        body: ListView(children: <Widget>[
          const ImageLogoGeneric(isBack: true),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Container(
              decoration: const BoxDecoration(
                color: NHColors.primaryBlackDetailColor,
              ),
              child: FormBuilder(
                key: _fKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .insertConfirmationCodeLabel,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .confirmationCodeEmailLabel,
                          style: TextStyle(
                            color: NHColors.whiteColor.withOpacity(0.7),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 30.0,
                        right: 30.0,
                      ),
                      child: PinCodeTextField(
                        appContext: context,
                        length: 4,
                        animationType: AnimationType.fade,
                        enableActiveFill: true,
                        onChanged: (value) {},
                        cursorColor: Colors.red,
                        textStyle: const TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                        ),
                        keyboardType: TextInputType.number,
                        pinTheme: PinTheme(
                          selectedColor: NHColors.primaryGreyColor,
                          selectedFillColor: NHColors.primaryGreyColor,
                          inactiveColor: NHColors.primaryGreyColor,
                          inactiveFillColor: NHColors.primaryGreyColor,
                          activeColor: NHColors.primaryGreyColor,
                          activeFillColor: NHColors.primaryGreyColor,
                          shape: PinCodeFieldShape.circle,
                        ),
                      ),
                    ),
                    const SizedBox(height: 100),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 80.0,
                        right: 80.0,
                      ),
                      child: NHButton(
                        title: AppLocalizations.of(context)!.continueStep,
                        disabled: true,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return const NewPasswordScreen();
                              },
                            ),
                          );
                        },
                        isExpanded: true,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
