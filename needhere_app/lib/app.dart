import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_builder_validators/localization/l10n.dart';
import 'package:get_it/get_it.dart';
import 'package:needhere_app/modules/screens/splash/screen.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:needhere_app/modules/user/presentation/cubit/authenticate/cubit.dart';

import 'app/routes.dart';
import 'modules/post/presentation/states/post/cubit.dart';
import 'modules/user/presentation/cubit/logout/cubit.dart';
import 'modules/user/presentation/cubit/register/cubit.dart';
import 'modules/user/presentation/cubit/validate/cubit.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticateCubit>(
          create: (context) => AuthenticateCubit(
            authenticateUseCase: GetIt.I(),
          ),
        ),
        BlocProvider<ValidateCubit>(
          create: (context) => ValidateCubit(
            validateUseCase: GetIt.I(),
          ),
        ),
        BlocProvider<LogoutCubit>(
          create: (context) => LogoutCubit(
            logoutUseCase: GetIt.I(),
          ),
        ),
        BlocProvider<RegisterCubit>(
          create: (context) => RegisterCubit(
            registerUseCase: GetIt.I(),
          ),
        ),
        BlocProvider<PostCubit>(
          create: (context) => PostCubit(
            getPostsUseCase: GetIt.I(),
          ),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Needhere App',
        theme: ThemeData(brightness: Brightness.dark, fontFamily: 'OpenSans'),
        initialRoute: SplashScreen.route,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          FormBuilderLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale('pt', ''),
          Locale('en', ''),
        ],
        routes: routes,
      ),
    );
  }
}
