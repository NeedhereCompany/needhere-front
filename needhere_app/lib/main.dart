import 'package:device_preview/device_preview.dart';
import 'package:flutter/widgets.dart';
import 'package:needhere_app/app.dart';
import 'package:needhere_app/app/providers/adapters.dart';
import 'package:needhere_app/app/providers/lazy.dart';
import 'package:needhere_app/app/providers/services.dart';
import 'package:needhere_app/app/services/service_locator/service_providers.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/providers/app.dart';
import 'app/providers/datasources.dart';
import 'app/providers/http.dart';
import 'app/providers/repositories.dart';
import 'app/providers/usecases.dart';

SharedPreferences? sharedPreferences;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  sharedPreferences = await SharedPreferences.getInstance();

  await ServiceProviders([
    HttpServiceProvider(),
    DataSourcesServiceProvider(),
    RepositoryServiceProvider(),
    UseCasesServiceProvider(),
    ExtraServicesServiceProvider(),
    AdaptersServiceProvider(),
    LazyServiceProvider()
  ]).boot();

  runApp(
    DevicePreview(
      enabled: false,
      builder: (context) => const App(),
    ),
  );
}
